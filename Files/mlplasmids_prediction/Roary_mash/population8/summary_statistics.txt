Core genes	(99% <= strains <= 100%)	26
Soft core genes	(95% <= strains < 99%)	67
Shell genes	(15% <= strains < 95%)	99
Cloud genes	(0% <= strains < 15%)	335
Total genes	(0% <= strains <= 100%)	527
