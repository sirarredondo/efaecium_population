Core genes	(99% <= strains <= 100%)	69
Soft core genes	(95% <= strains < 99%)	83
Shell genes	(15% <= strains < 95%)	246
Cloud genes	(0% <= strains < 15%)	642
Total genes	(0% <= strains <= 100%)	1040
