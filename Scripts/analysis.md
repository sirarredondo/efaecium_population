The pan-plasmidome of the major nosocomial pathogen Enterococcus faecium
================
Sergio Arredondo-Alonso

# Setting up the analysis

## Working directory

``` r
setwd("~/Data/efaecium_population/Scripts")
```

## R libraries

``` r
library(ggplot2)
library(ggtree)
library(tidyverse)
library(ape)
library(cowplot)
library(UpSetR)
library(reshape2)
library(ggpubr)
library(phangorn)
library(factoextra)
library(RColorBrewer)
library(gplots)
library(Biostrings)
library(fitdistrplus)
```

## Metadata file

``` r
metadata <- read.csv("../Files/Metadata/metadata.csv", header = T, stringsAsFactors = F, sep = ",", row.names = NULL)
colnames(metadata)[2] <- 'Strain'

clean_dates <- which(metadata$year == '1999-2000')
metadata$year[clean_dates] <- '1999'


all_clades_metadata <- read.csv('../Files/Metadata/metadata_cladeA_cladeB.csv', header = T, stringsAsFactors = F, sep = ",", row.names = NULL)

clade_B_metadata <- subset(all_clades_metadata,! all_clades_metadata$ID %in% unique(metadata$Strain))
clade_A_metadata <- subset(all_clades_metadata, all_clades_metadata$ID %in% unique(metadata$Strain))

write.csv(x = clade_A_metadata, file = '../Files/Metadata/cladeA_metadata.csv', quote = FALSE, row.names = FALSE)
```

# ML core-genome visualization

``` r
core_genome_tree <- read.tree('../Files/Phylogenies/core_genome/core_genome_tree.nwk')
```

## Tree visualization

``` r
# Core genome visualization

core_genome_tree <- read.tree('../Files/Phylogenies/core_genome/core_genome_tree.nwk')

ggtree_bionj <- ggtree(core_genome_tree, layout = 'rectangular')

extract.clade(core_genome_tree, 1661) # Internal node used to define clade A1
```

    ## 
    ## Phylogenetic tree with 1227 tips and 1226 internal nodes.
    ## 
    ## Tip labels:
    ##  E7485, E7588, E7333, E7413, E7368, E7581, ...
    ## 
    ## Rooted; includes branch lengths.

``` r
ggtree_bionj <- ggtree(core_genome_tree, layout = 'rectangular') + geom_hilight(node = 1661, fill = 'lemonchiffon2') + geom_cladelabel(node = 1661, label = 'clade A1', align = T, geom='label', hjust = 1.5)


labels_tree <- ggtree_bionj$data$label
info_strains <- metadata[metadata$Strain %in% labels_tree,]
  

tree_add_info <- data.frame(label = as.character(info_strains$Strain),
             Feature = info_strains$source_characteristic)

tree_add_info$Feature <- as.character(tree_add_info$Feature)

index_miscellaneous <- which(! tree_add_info$Feature %in% c('Hospitalized patient','Non-hospitalized person','Pig','Poultry','Pet'))
tree_add_info$Feature[index_miscellaneous] <- 'Others'

tree_heatmap <- facet_plot(ggtree_bionj, panel = 'Source', data = tree_add_info, geom=geom_tile, aes(x= as.numeric(as.factor(Feature)),fill=Feature)) + geom_treescale() + theme_tree2(legend.position='bottom') + scale_fill_manual(values = c("#dc143c", "#0000ff", "black", "#9acd32", "#ff69b4","#b8860b"))      

tree_heatmap
```

![](analysis_files/figure-gfm/unnamed-chunk-6-1.png)<!-- -->

## Source distribution between clade A1

``` r
cladeA1_metadata <- read_csv(file = '../Files/Metadata/cladeA1_metadata.csv')

other_index <- which(! cladeA1_metadata$source_characteristic %in% c('Hospitalized patient','Non-hospitalized person','Pet','Pig','Poultry'))
cladeA1_metadata$source_characteristic[other_index] <- 'Others'

df_hist_clade <- count(cladeA1_metadata, source_characteristic, CladeA1)

df_hist_clade <- df_hist_clade %>%
  group_by(source_characteristic) %>%
  mutate(perc = n/sum(n))

hist_clade <- ggplot(df_hist_clade, aes(x = as.factor(df_hist_clade$source_characteristic), y = df_hist_clade$perc*100, fill = as.factor(df_hist_clade$CladeA1)), color = 'black') +
geom_bar(stat="identity", width = 0.7) + scale_fill_manual(values = c('lemonchiffon4','lemonchiffon2')) + coord_flip() + geom_text(data = df_hist_clade, mapping = aes(x = as.factor(source_characteristic), y = perc*100, label = n)) +
labs(x = '',y= 'Percentage', fill = 'Clade A1') + theme(legend.position = 'right', axis.text.x = NULL) + theme_classic(base_size = 18) 

hist_clade
```

![](analysis_files/figure-gfm/unnamed-chunk-7-1.png)<!-- -->

# Complete plasmid sequences

## Unicycler - Plasmid Annotation

``` r
##################### Unicycler information

# Loading information derived from Unicycler regarding contigs assembled 

unicycler_replicon_info <- read.table('../Files/Unicycler_assemblies/replicon_info_unicycler.csv',sep = ' ') 

colnames(unicycler_replicon_info) <- c('Strain','Replicon_name','Length','Depth_estimation','Circularity')

# Chromosomal contigs were only defined if they had a contig length higher than 350 kbp

chr_seq <- subset(unicycler_replicon_info, unicycler_replicon_info$Length > 350000)

levels(factor(chr_seq$Strain))
```

    ##  [1] "E0139" "E0595" "E0656" "E1334" "E1774" "E2079" "E2364" "E4227"
    ##  [9] "E4239" "E4402" "E4413" "E4438" "E4456" "E4457" "E6020" "E6043"
    ## [17] "E6055" "E6975" "E6988" "E7025" "E7040" "E7067" "E7070" "E7098"
    ## [25] "E7114" "E7160" "E7171" "E7196" "E7199" "E7207" "E7237" "E7240"
    ## [33] "E7246" "E7313" "E7356" "E7357" "E7429" "E7441" "E7471" "E7591"
    ## [41] "E7654" "E7663" "E7933" "E7948" "E8014" "E8040" "E8172" "E8195"
    ## [49] "E8202" "E8284" "E8290" "E8328" "E8377" "E8407" "E8414" "E8423"
    ## [57] "E8440" "E8481" "E8691" "E8867" "E8927" "E9101"

``` r
length(levels(factor(chr_seq$Strain)))
```

    ## [1] 62

``` r
# Plasmid contigs are defined based on size (< 350 kbp) and if they present circularization signatures. Rest of the contigs are catalogued as 'Undetermined'

unicycler_replicon_info$type <- rep('Undetermined',nrow(unicycler_replicon_info))

unicycler_replicon_info$type <- ifelse(unicycler_replicon_info$Length > 350000, 'Chromosome',ifelse(unicycler_replicon_info$Length < 350000 & unicycler_replicon_info$Circularity == 'true', 'Plasmid', 'Undetermined'))


# Size of the chromosomes present in our dataset 

sizes_chr <- subset(unicycler_replicon_info, unicycler_replicon_info$type == 'Chromosome' & unicycler_replicon_info$Circularity == 'true')

# Minimum chromosome length found in our assemblies

round(x = (min(sizes_chr$Length)/1e6), digits = 2)
```

    ## [1] 2.42

``` r
# Maximum chromosome length found in our assemblies

round(x = (max(sizes_chr$Length)/1e6), digits = 2)
```

    ## [1] 3.01

``` r
# Mean chromosome length of hospitalized isolates 

metadata_sizes_chr <- merge(metadata, sizes_chr, by = 'Strain')

metadata_sizes_chr %>%
  count(isolation.source)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["isolation.source"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"Chicken","2":"2"},{"1":"Dog","2":"7"},{"1":"Hospitalized patient","2":"32"},{"1":"Non-hospitalized person","2":"3"},{"1":"Pig","2":"4"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
metadata_sizes_chr %>%
  group_by(isolation.source) %>%
  summarise(mean_chr_length = mean(Length)/1e6)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["isolation.source"],"name":[1],"type":["chr"],"align":["left"]},{"label":["mean_chr_length"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"Chicken","2":"2.418680"},{"1":"Dog","2":"2.656164"},{"1":"Hospitalized patient","2":"2.823217"},{"1":"Non-hospitalized person","2":"2.470538"},{"1":"Pig","2":"2.546788"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Based on contig annotation we filtered out phage sequences (n = 6)

unicycler_replicon_info <- unicycler_replicon_info[!unicycler_replicon_info$Replicon_name %in% c('E6043_5','E7114_5','E8440_3','E8481_3','E7160_6','E7246_7'),]

# Now we have cleaned our dataframe from the presence of some phages that we detected. We check how many strains have at least one plasmid 

plas_seq <- subset(unicycler_replicon_info, unicycler_replicon_info$type == 'Plasmid')

# Number of circular plasmids
length(levels(factor(plas_seq$Replicon_name)))
```

    ## [1] 305

``` r
# Minimum plasmid length found in our assemblies

round(x = (min(plas_seq$Length)/1e3), digits = 2)
```

    ## [1] 1.93

``` r
# Maximum plasmid length found in our assemblies

round(x = (max(plas_seq$Length)/1e3), digits = 2)
```

    ## [1] 293.85

``` r
# Stats from the complete plasmid sequences (circular!)

summary(plas_seq$Length)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##    1928    5088   15151   53484   58508  293851

``` r
# Mean length of the complete plasmid sequences

round(mean(plas_seq$Length)/1e3,2)
```

    ## [1] 53.48

``` r
# Median length of the complete plasmid sequences

round(median(plas_seq$Length)/1e3,2)
```

    ## [1] 15.15

``` r
# Mean chromosome length of hospitalized isolates 

metadata_sizes_pl <- merge(metadata, plas_seq, by = 'Strain')

unique_isolates_pl <- metadata_sizes_pl[! duplicated(metadata_sizes_pl$Strain),]

unique_isolates_pl %>%
  count(isolation.source)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["isolation.source"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"Chicken","2":"2"},{"1":"Dog","2":"9"},{"1":"Hospitalized patient","2":"43"},{"1":"Non-hospitalized person","2":"2"},{"1":"Pig","2":"3"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Number of plasmids per strain

counts_plasmids <- plas_seq %>%
  count(Strain)

total_length_plasmids <- plas_seq %>%
  group_by(Strain) %>%
  summarize(total_pl_length = sum(Length))

tidy_plasmids <- merge(counts_plasmids, total_length_plasmids, by = 'Strain')

info_counts <- merge(tidy_plasmids, metadata, by = 'Strain')


## Second graph showing distribution of strains based on pl length and number

info_counts$total_pl_length <- info_counts$total_pl_length/1e3

plot_distribution_isolate <- ggplot(info_counts, aes(x= info_counts$n, y = info_counts$total_pl_length, group = info_counts$isolation.source, color = info_counts$isolation.source, fill = info_counts$isolation.source)) + 
        geom_point(size = 3.0) + 
        labs(x='Number of plasmids',y='Cumulative plasmid length (kbp)') +
        scale_colour_manual(values=c("#dc143c", "#0000ff", "#9acd32", "#ff69b4","#b8860b")) +
        scale_fill_manual(values=c("#dc143c", "#0000ff", "#9acd32", "#ff69b4","#b8860b")) +
        theme(legend.position = 'none') +
        theme(legend.title=element_blank()) +
        scale_x_continuous(breaks=c(0,1,2,3,4,5,6,7,8,9,10,11)) 

info_counts$isolation.source <- factor(info_counts$isolation.source, levels = c('Hospitalized patient','Non-hospitalized person','Dog','Pig','Chicken'))



boxplot_distribution_isolate <- ggplot(info_counts, aes(x= info_counts$isolation.source, y = info_counts$total_pl_length, group = info_counts$isolation.source, color = info_counts$isolation.source, fill = info_counts$isolation.source)) + geom_boxplot(alpha = 0.1) + geom_point() +
        scale_colour_manual(values=c("#dc143c", "#0000ff", "#9acd32", "#ff69b4","#b8860b")) +
        scale_fill_manual(values=c("#dc143c", "#0000ff", "#9acd32", "#ff69b4","#b8860b")) + labs(x='',y='Cumulative plasmid length (kbp)', title = '') + theme(axis.text.x = NULL, legend.position = 'none')

plot_grid(plot_distribution_isolate, boxplot_distribution_isolate, nrow = 2)
```

![](analysis_files/figure-gfm/unnamed-chunk-8-1.png)<!-- -->

``` r
# Stats for the manuscript
  
info_counts %>%
  group_by(isolation.source) %>%
  summarize(total_pl = sum(n),
    mean_pl_count = mean(n),
            median_pl_count = median(n),
            mean_cumulative_pl = mean(total_pl_length),
            median_cumulative_pl = median(total_pl_length))
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["isolation.source"],"name":[1],"type":["fctr"],"align":["left"]},{"label":["total_pl"],"name":[2],"type":["int"],"align":["right"]},{"label":["mean_pl_count"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["median_pl_count"],"name":[4],"type":["dbl"],"align":["right"]},{"label":["mean_cumulative_pl"],"name":[5],"type":["dbl"],"align":["right"]},{"label":["median_cumulative_pl"],"name":[6],"type":["dbl"],"align":["right"]}],"data":[{"1":"Hospitalized patient","2":"245","3":"5.697674","4":"6.0","5":"306.2273","6":"306.0990"},{"1":"Non-hospitalized person","2":"3","3":"1.500000","4":"1.5","5":"149.8525","6":"149.8525"},{"1":"Dog","2":"46","3":"5.111111","4":"5.0","5":"206.3214","6":"192.9490"},{"1":"Pig","2":"5","3":"1.666667","4":"1.0","5":"180.0767","6":"189.3980"},{"1":"Chicken","2":"6","3":"3.000000","4":"3.0","5":"223.9455","6":"223.9455"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
info_counts %>%
  count(isolation.source)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["isolation.source"],"name":[1],"type":["fctr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"Hospitalized patient","2":"43"},{"1":"Non-hospitalized person","2":"2"},{"1":"Dog","2":"9"},{"1":"Pig","2":"3"},{"1":"Chicken","2":"2"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
counts_plasmids
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Strain"],"name":[1],"type":["fctr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"E0139","2":"1"},{"1":"E0595","2":"1"},{"1":"E0656","2":"1"},{"1":"E1334","2":"2"},{"1":"E1774","2":"3"},{"1":"E4227","2":"3"},{"1":"E4239","2":"3"},{"1":"E4402","2":"4"},{"1":"E4413","2":"5"},{"1":"E4438","2":"4"},{"1":"E4456","2":"5"},{"1":"E4457","2":"11"},{"1":"E6020","2":"8"},{"1":"E6043","2":"5"},{"1":"E6055","2":"5"},{"1":"E6975","2":"8"},{"1":"E6988","2":"9"},{"1":"E7025","2":"8"},{"1":"E7040","2":"6"},{"1":"E7067","2":"7"},{"1":"E7070","2":"6"},{"1":"E7098","2":"2"},{"1":"E7114","2":"9"},{"1":"E7160","2":"6"},{"1":"E7171","2":"5"},{"1":"E7196","2":"5"},{"1":"E7199","2":"5"},{"1":"E7207","2":"2"},{"1":"E7237","2":"3"},{"1":"E7240","2":"9"},{"1":"E7246","2":"6"},{"1":"E7313","2":"2"},{"1":"E7356","2":"4"},{"1":"E7357","2":"4"},{"1":"E7429","2":"8"},{"1":"E7441","2":"5"},{"1":"E7471","2":"6"},{"1":"E7591","2":"4"},{"1":"E7654","2":"6"},{"1":"E7663","2":"6"},{"1":"E7933","2":"9"},{"1":"E7948","2":"6"},{"1":"E8014","2":"5"},{"1":"E8040","2":"4"},{"1":"E8172","2":"11"},{"1":"E8195","2":"7"},{"1":"E8202","2":"5"},{"1":"E8284","2":"5"},{"1":"E8290","2":"6"},{"1":"E8328","2":"4"},{"1":"E8377","2":"6"},{"1":"E8407","2":"2"},{"1":"E8414","2":"7"},{"1":"E8423","2":"2"},{"1":"E8440","2":"7"},{"1":"E8481","2":"3"},{"1":"E8691","2":"7"},{"1":"E8867","2":"1"},{"1":"E8927","2":"6"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

## Plasmid annotation

``` r
##################### Plasmid annotation

# Loading after blasting with Abricate using the Rip database

abricate_rip <- read_csv(file = '../Files/Plasmid_annotation/Abricate_Rip.csv')
```

    ## Parsed with column specification:
    ## cols(
    ##   `#FILE` = col_character(),
    ##   SEQUENCE = col_character(),
    ##   START = col_double(),
    ##   END = col_double(),
    ##   GENE = col_character(),
    ##   COVERAGE = col_character(),
    ##   COVERAGE_MAP = col_character(),
    ##   GAPS = col_character(),
    ##   `%COVERAGE` = col_double(),
    ##   `%IDENTITY` = col_double(),
    ##   DATABASE = col_character(),
    ##   ACCESSION = col_logical(),
    ##   PRODUCT = col_character()
    ## )

``` r
################################################## Similar Rip groups 

## Applying a filter of minimum 60% coverage and 80% identity

filt_abricate_rip <- subset(abricate_rip, abricate_rip$`%COVERAGE` > 60 & abricate_rip$`%IDENTITY` > 80)
filt_abricate_rip$rip <- str_split_fixed(filt_abricate_rip$PRODUCT, "\\|", 2)[,2]
colnames(filt_abricate_rip)[2] <- 'Plasmid'


filt_abricate_rip$rep_accession <- str_split_fixed(filt_abricate_rip$GENE, "\\|", 3)[,2]


# We load an extra dataframe in which we calculated some statistics from the complete plasmid sequences

plasmid_stats <- read.table(file = '../Files/Plasmid_annotation/plasmid_annotation_info.txt', sep = '\t')
colnames(plasmid_stats) <- c('Strain','Plasmid','Length','GC_content','Genes','perc_genes','Rep_group','Mob_group','mpf_group','predicted_transferability','draft_resistance')


# There is also a column in which it is indicated the Rip group, however it is more accurate the procedure that we followed with Abricate. Same applies for the Mob and Mpf group. 

plasmid_stats$Rep_group <- NULL
plasmid_stats$Mob_group <- NULL
plasmid_stats$mpf_group <- NULL


# Plasmid accesions + Names

# The following file have the information regarding for each Rip group in which complete plasmid sequence was described (This information is present in Table 1 from Clewell et al. 2014)

pl_acc_names <- read.table(file = '../Files/Plasmid_annotation/rip_accessions.tsv', header = FALSE, sep = '\t')
colnames(pl_acc_names) <- c('rep_family','pl_name','pl_accession','organism','rep_name','rep_accession')

pl_acc_names$rep_accession <- gsub(pattern = '\\.[^.]+',replacement = '', x = pl_acc_names$rep_accession)

# We merge the results from Abricate with the previous dataframe to have a complete overview of the Rip groups that we identify in our complete plasmid sequences and in which organism where previously described

rip_complete_info <- merge(filt_abricate_rip, pl_acc_names, by = 'rep_accession')

# Count rip families

rip_complete_info %>%
  count(rip)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["rip"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"Inc18","2":"60"},{"1":"Rep_1","2":"8"},{"1":"Rep_3","2":"56"},{"1":"RepA_N","2":"82"},{"1":"Rep_trans","2":"24"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Counting rip families and most similar known rip (described in Clewell et al. 2014) present in our collection

known_rep_pl <- rip_complete_info %>%
  group_by(rip) %>%
  count(pl_name)

# Average plasmid length based on rip family

pl_features_info <- merge(rip_complete_info, plasmid_stats, by = 'Plasmid')

################################################# Non-similar rip sequences 

# Some plasmid sequences do not have a similar match to our rip database

unannotated_pl <- setdiff(plas_seq$Replicon_name, filt_abricate_rip$Plasmid) # So in total 102 pl sequences are not annotated in the first check

# We can annotate most of the remaining pl sequences setting lower the %ID threshold and calling them as plasmid-like

like_abricate_rip <- subset(abricate_rip, abricate_rip$`%COVERAGE` > 60 & abricate_rip$`%IDENTITY` < 80)

like_abricate_rip$rip <- str_split_fixed(like_abricate_rip$PRODUCT, "\\|", 2)[,2]

like_abricate_rip$rip <- paste(like_abricate_rip$rip, '-like', sep = '')

like_abricate_rip$rep_accession <- str_split_fixed(like_abricate_rip$GENE, "\\|", 3)[,2]

colnames(like_abricate_rip)[2] <- 'Plasmid'

like_rip_complete_info <- merge(like_abricate_rip, pl_acc_names, by = 'rep_accession') # In this dataframe we also have plasmid sequences previously defined with a known rip. Main issue is that a plasmid sequence can contain a Rip and Rip-like. 

# If we sum the following

nrow(rip_complete_info) + nrow(like_rip_complete_info) # 357, indicating there are plasmid sequences with multirep. Later on we will remove duplicates based on Plasmid and concatena rip families in case of multireplicon families
```

    ## [1] 357

``` r
colnames(like_rip_complete_info)[3] <- 'Plasmid'

# Count rip families

like_rip_complete_info %>%
  count(rip)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["rip"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"Rep_1-like","2":"6"},{"1":"Rep_2-like","2":"10"},{"1":"Rep_3-like","2":"55"},{"1":"RepA_N-like","2":"20"},{"1":"Rep_trans-like","2":"36"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Counting rip families and most similar known rep present in our collection

like_known_rep_pl <- like_rip_complete_info %>%
  group_by(rip) %>%
  count(pl_name)

# Average plasmid length based on rip family

like_pl_features_info <- merge(like_rip_complete_info, plasmid_stats, by = 'Plasmid')

#################################### Considering plasmid and plasmid-like

# We merge both dataframes based on Plasmid, here we try to remove duplicates and concatenate rips if there is presence of multirep plasmids. 

all_pl_features <- rbind(pl_features_info, like_pl_features_info) # 357 Rips

multireplicon_df <- NULL
for(plasmid in all_pl_features$Plasmid)
{
  pl_df <- subset(all_pl_features, all_pl_features$Plasmid == plasmid) # We select a unique plasmid 
  pl_df$rip <- gsub(pattern = ' ',replacement = '', pl_df$rip) # We clean the field of rip
  replicons_element <- paste(pl_df$rip, collapse = ',') # We clean the field of rip
  multireplicon_df <- append(x = multireplicon_df, values = replicons_element, after = length(multireplicon_df)) # We concatenate all the rips
}

length(multireplicon_df) # 357 elements in the vector 
```

    ## [1] 357

``` r
all_pl_features$all_replicons <- multireplicon_df # We add this column to the previous dataframe

# Now the important step, here we remove duplicates but only because we have incorporated all the Rips present per plasmid

non_redundant_pl_features <- all_pl_features[! duplicated(all_pl_features$Plasmid), ] 

non_redundant_pl_features$all_replicons <- sapply(lapply(sapply(non_redundant_pl_features$all_replicons, strsplit, ","), sort), paste, collapse = ",")

dim(non_redundant_pl_features) # We check we have 294 plasmids with 11 which remained unannotated 
```

    ## [1] 294  28

``` r
# 11 plasmids do not have a RIP family associated

unannotated_plasmids <-setdiff(plas_seq$Replicon_name, non_redundant_pl_features$Plasmid)

features_unannotated <- plasmid_stats[plasmid_stats$Plasmid %in% unannotated_plasmids,]

round(mean(features_unannotated$Length)/1000,2)
```

    ## [1] 4.11

``` r
################################ Stats and metrics reported in the paper

# Average pl length by Rep group

rep_df_pl_length_stats <- NULL
rep_pl_length <- function(rep)
{
  rep_group <<- non_redundant_pl_features[grep(pattern = rep, x = non_redundant_pl_features$all_replicons),]
  mean_pl_length <- round(mean(rep_group$Length)/1000,2)
  rep_pl_length_stats <- c(rep, mean_pl_length)
  rep_df_pl_length_stats <<- rbind(rep_df_pl_length_stats, rep_pl_length_stats)
}

rep_pl_length(rep = "RepA_N$")
rep_pl_length(rep = "RepA_N-like")
rep_pl_length(rep = "Inc18")
rep_pl_length(rep = "Rep_3$")
rep_pl_length(rep = "Rep_3-like")
rep_pl_length(rep = "Rep_trans$")
rep_pl_length(rep = "Rep_trans-like")
rep_pl_length(rep = "Rep_1$")
rep_pl_length(rep = "Rep_1-like")
rep_pl_length(rep = "Rep_2-like")

rep_df_pl_length_stats[,1] <- gsub("\\$","", rep_df_pl_length_stats[,1])
colnames(rep_df_pl_length_stats) <- c("Replicon","Pl_mean_length")

# All the combinations that we could draw from the rip sequences

report_multireplicon <- non_redundant_pl_features %>%
  count(all_replicons)


non_redundant_pl_features %>%
  group_by(rip) %>%
  summarize(mean_pl_length = mean(Length)/1e3)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["rip"],"name":[1],"type":["chr"],"align":["left"]},{"label":["mean_pl_length"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"Inc18","2":"42.692250"},{"1":"Rep_1","2":"38.625800"},{"1":"Rep_1-like","2":"4.008667"},{"1":"Rep_2-like","2":"3.440250"},{"1":"Rep_3","2":"10.279245"},{"1":"Rep_3-like","2":"5.611297"},{"1":"RepA_N","2":"152.627416"},{"1":"RepA_N-like","2":"52.896059"},{"1":"Rep_trans","2":"22.832909"},{"1":"Rep_trans-like","2":"3.231600"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Estimating frequency of being in multireplicon plasmids per Rep group

multirep_stats <- NULL

freq_multirep <- function(rep_parsing, rep, rep_like)
{
  # Counting number of times a Rep appears alone (not present in a multireplicon plasmid)
  only_rep <<- report_multireplicon[grep(pattern = rep_parsing, x = report_multireplicon$all_replicons),]
  numb_only_rep <- sum(only_rep$n)

  # Counting number of times a Rep or Rep-like appears
  pl_with_rep <<- report_multireplicon[grep(pattern = rep, x = report_multireplicon$all_replicons),]
  numb_pl_with_rep <- sum(pl_with_rep$n)

  # Counting number of times Rep-like appears 
  pl_with_rep_like <- report_multireplicon[grep(pattern = rep_like, x = report_multireplicon$all_replicons),]
  numb_pl_with_rep_like <- sum(pl_with_rep_like$n)
  
  # Counting number of times Rep and Rep-like appears in the same multireplicon plasmid
  
  both_rep_replike <<- paste(rep, rep_like, sep = ',')
  
  pl_with_both_rep_replike <<- report_multireplicon[grep(pattern = both_rep_replike, x = report_multireplicon$all_replicons),]
  numb_pl_both_rep_replike <- sum(pl_with_both_rep_replike$n)
  numb_pl_with_rep_like <- numb_pl_with_rep_like - numb_pl_both_rep_replike
  
  # Counting number of times Rep appears
  numb_pl_with_rep <- (numb_pl_with_rep - numb_pl_with_rep_like)
  
  numb_pl_multirep <- numb_pl_with_rep - numb_only_rep
  
  freq_multirep <- numb_pl_multirep/numb_pl_with_rep
  
  freq_multirep_round <- round(freq_multirep,2)
  
  numb_pl_multirep <- numb_pl_with_rep - numb_only_rep
  rep_stats <- c(rep, numb_pl_with_rep, numb_only_rep, numb_pl_multirep, freq_multirep_round)
  multirep_stats <<- rbind(multirep_stats, rep_stats)
}

freq_multirep(rep_parsing = '^RepA_N$', rep = 'RepA_N', rep_like = 'RepA_N-like')
freq_multirep(rep_parsing = '^Inc18$', rep = 'Inc18', rep_like = 'Inc18-like')
freq_multirep(rep_parsing = '^Rep_3$', rep = 'Rep_3', rep_like = 'Rep_3-like')
freq_multirep(rep_parsing = '^Rep_trans$', rep = 'Rep_trans', rep_like = 'Rep_trans-like')
freq_multirep(rep_parsing = '^Rep_1$', rep = 'Rep_1', rep_like = 'Rep_1-like')

colnames(multirep_stats) <- c('Replicon','Rep_counts','Only_Rep','Multirep','Freq_multirep') # This gives us the numbers that we are later on reporting in the manuscript based on plasmid


# Estimating the frequency of being in multireplicon plasmids per Rep-like group

multireplike_stats <- NULL

freq_multireplike <- function(replike_parsing, replike)
{
  # Counting number of times a Rep appears alone (not present in a multireplicon plasmid)
  only_replike <- report_multireplicon[grep(pattern = replike_parsing, x = report_multireplicon$all_replicons),]
  numb_only_replike <- sum(only_replike$n)

  # Counting number of times Rep-like appears
  pl_with_replike <- report_multireplicon[grep(pattern = replike, x = report_multireplicon$all_replicons),]
  numb_pl_with_replike <- sum(pl_with_replike$n)

  numb_pl_multireplike <- numb_pl_with_replike - numb_only_replike
  
  freq_multireplike <- numb_pl_multireplike/numb_pl_with_replike
  
  freq_multireplike_round <- round(freq_multireplike,2)
  
  replike_stats <- c(replike, numb_pl_with_replike, numb_only_replike, numb_pl_multireplike, freq_multireplike_round)
  multireplike_stats <<- rbind(multireplike_stats, replike_stats)
}


freq_multireplike(replike_parsing = '^RepA_N-like$', replike = 'RepA_N-like')
freq_multireplike(replike_parsing = '^Rep_3-like$', replike = 'Rep_3-like')
freq_multireplike(replike_parsing = '^Rep_trans-like$', replike = 'Rep_trans-like')
freq_multireplike(replike_parsing = '^Rep_1-like$', replike = 'Rep_1-like')
freq_multireplike(replike_parsing = '^Rep_2-like$', replike = 'Rep_2-like')

colnames(multireplike_stats) <- c('Replicon','Rep_counts','Only_Rep','Multirep','Freq_multirep')

all_multirep_stats <- rbind(multirep_stats, multireplike_stats)

report_pl_rep_stats <- merge(all_multirep_stats, rep_df_pl_length_stats, by = "Replicon") # Here we include the mean plasmid length in which each of the RIP is found 


# Rip features 

known_rep_pl <- all_pl_features %>%
  group_by(rip) %>%
  count(pl_name)

counts_single_rip <- all_pl_features %>%
  count(rip)

counts_single_rip$rip <- gsub(pattern = ' ', replacement = '', x = counts_single_rip$rip)


# Mean plasmid length were the rips are found

all_pl_features %>%
   group_by(rip) %>%
   summarize(mean(Length))
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["rip"],"name":[1],"type":["chr"],"align":["left"]},{"label":["mean(Length)"],"name":[2],"type":["dbl"],"align":["right"]}],"data":[{"1":"Inc18","2":"44736.483"},{"1":"Rep_1","2":"36364.375"},{"1":"Rep_1-like","2":"4008.667"},{"1":"Rep_2-like","2":"23995.600"},{"1":"Rep_3","2":"12382.232"},{"1":"Rep_3-like","2":"17096.582"},{"1":"RepA_N","2":"151282.220"},{"1":"RepA_N-like","2":"53892.800"},{"1":"Rep_trans","2":"25660.125"},{"1":"Rep_trans-like","2":"20762.028"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# Metadata information

metadata_pl <- merge(all_pl_features, metadata, by = 'Strain')

# Stats for incomp. groups

metadata_rep <- metadata_pl %>%
  group_by(isolation.source) %>%
  count(rip)



################################## Relaxases

# Loading relaxases annotation 

abricate_rel <- read_csv(file = '/home/sergi/Data/Paper_Data/Circular_plasmids/Abricate_Rel.csv')
```

    ## Parsed with column specification:
    ## cols(
    ##   `#FILE` = col_character(),
    ##   SEQUENCE = col_character(),
    ##   START = col_double(),
    ##   END = col_double(),
    ##   GENE = col_character(),
    ##   COVERAGE = col_character(),
    ##   COVERAGE_MAP = col_character(),
    ##   GAPS = col_character(),
    ##   `%COVERAGE` = col_double(),
    ##   `%IDENTITY` = col_double(),
    ##   DATABASE = col_character(),
    ##   ACCESSION = col_logical(),
    ##   PRODUCT = col_character()
    ## )

``` r
## Filtering based on a minimum 60% coverage and 80% identity

filt_abricate_rel <- subset(abricate_rel, abricate_rel$`%COVERAGE` > 60 & abricate_rel$`%IDENTITY` > 80)
filt_abricate_rel$rel <- str_split_fixed(filt_abricate_rel$PRODUCT, "\\|", 2)[,2]
colnames(filt_abricate_rel)[2] <- 'Plasmid'

rel_info <- subset(filt_abricate_rel, select = c('Plasmid','rel'))

# Rip and Relaxases information

rel_rip <- merge(rel_info, non_redundant_pl_features, by = 'Plasmid')

rel_rip <- rel_rip[! duplicated(rel_rip),]

rel_rip %>%
  count(rel)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["rel"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"MOB_C","2":"2"},{"1":"MOB_P","2":"124"},{"1":"MOB_T","2":"6"},{"1":"MOB_V","2":"29"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
rel_per_rip <- rel_rip %>%
  group_by(rel) %>%
  count(all_replicons)
```

## Visualizing all Rip and Rel combinations

``` r
## Graph showing plasmid replication initiator genes (rip)

# Creating an upsetR figure

pl_features <- subset(all_pl_features, select = c("Plasmid","rip"))
pl_features <- pl_features[! duplicated(pl_features),]

unique_values_pl <- unique(pl_features)
upset_pl_features <- dcast(unique_values_pl, formula = Plasmid ~ rip, fun.aggregate = length)
```

    ## Using rip as value column: use value.var to override.

``` r
inc18 <- subset(upset_pl_features, upset_pl_features$`Rep_1` == 1)

colnames(upset_pl_features) <- gsub(pattern = " ", replacement = "", x = colnames(upset_pl_features))

mean_pl_rip <- data.frame(sets = rep_df_pl_length_stats[,1],
              Average_plasmid_length = rep_df_pl_length_stats[,2])
mean_pl_rip$Average_plasmid_length <- as.numeric(as.character(mean_pl_rip$Average_plasmid_length))

upset(upset_pl_features, nsets = 10, number.angles = 0, point.size = 2.0 , line.size = 0.0, 
    mainbar.y.label = "Number of plasmids per combination", sets.x.label = "Number of plasmids per Rip", order.by = "freq",
    set.metadata = list(data = mean_pl_rip, plots = list(list(type = "hist", 
    column = "Average_plasmid_length", assign = 25))))
```

![](analysis_files/figure-gfm/unnamed-chunk-10-1.png)<!-- -->

``` r
# Incorporating relaxase information 

rel_features <- subset(rel_rip, select = c("Plasmid", "rel"))
rel_features$rel <- gsub(pattern = " ", replacement = "", x = rel_features$rel)

unique_values_rel <- unique(rel_features)
upset_rel_features <- dcast(unique_values_rel, formula = Plasmid ~ rel, fun.aggregate = length)
```

    ## Using rel as value column: use value.var to override.

``` r
upset(upset_rel_features, nsets = 10, number.angles = 2, point.size = 1.0 , line.size = 0, 
    mainbar.y.label = "Number of plasmids per combination", sets.x.label = "Number of plasmids per Relaxase", order.by = "freq")
```

![](analysis_files/figure-gfm/unnamed-chunk-10-2.png)<!-- -->

``` r
# Combining both figures

colnames(rel_features)[2] <- 'rip'

all_features <- rbind(pl_features, rel_features)
all_features <- all_features[! duplicated(all_features),]

unique_values_all_features <- unique(all_features)
upset_all_features <- dcast(unique_values_all_features, formula = Plasmid ~ rip, fun.aggregate = length)
```

    ## Using rip as value column: use value.var to override.

``` r
colnames(upset_all_features) <- gsub(pattern = " ", replacement = "", x = colnames(upset_all_features))

colnames(upset_all_features) <- gsub(pattern = "-", replacement = "_", x = colnames(upset_all_features))

mean_pl_rip$sets <- gsub(pattern = "-", replacement = "_", x = mean_pl_rip$sets)

upset(upset_all_features, 
      nintersects = 1000,
      sets = c("RepA_N","Inc18","Rep_3","Rep_trans","Rep_1","RepA_N_like","Rep_trans_like","Rep_3_like","Rep_2_like","Rep_1_like","MOB_P","MOB_V","MOB_T","MOB_C"),
      nsets = 14, 
      number.angles = 2, 
      point.size = 2.0 , 
      line.size = 0.0, 
      mainbar.y.label = "Number of plasmids per combination",
      sets.x.label = "Number of plasmids per RIP or MOB",
      keep.order = TRUE,
      order.by = 'freq',
      matrix.color = 'coral2',
      main.bar.color = 'black',
    set.metadata = list(data = mean_pl_rip, plots = list(list(type = "hist", 
    column = "Average_plasmid_length", assign = 25))))
```

![](analysis_files/figure-gfm/unnamed-chunk-10-3.png)<!-- -->

## Distance matrix of complete plasmid sequences (Mash analysis)

``` r
# Loading Mash distances from complete plasmid sequences

parsing_mash <- read.table(file = '../Files/Plasmid_annotation/mash_dist_complete_pl.txt')

# First column contains the name of the complete plasmid sequences
pl_concatenated <- parsing_mash[,1]
len_strains <- length(unique(pl_concatenated))

# We transform this file into a distance matrix 
mash_df <- NULL
first_strain <- as.character(parsing_mash[1,1])
for(i in 1:length(pl_concatenated))
{
  if(parsing_mash[i,1]== first_strain)
  {
    first_index <- i
    second_index <- first_index + len_strains
    df_selection <- parsing_mash[c(first_index:second_index),2]
    mash_df <- rbind(mash_df, df_selection)
  }
}
mash_df <- mash_df[,c(1:len_strains)]

# We clean the name of the plasmids 

plasmids <- unique(as.character(parsing_mash[,1]))
plasmids <- gsub(".fasta","",plasmids)

parsing_pl <- str_split_fixed(string = plasmids, pattern = "\\.", 2)

# Adding clean plasmid names

rownames(mash_df) <- parsing_pl[,1]
colnames(mash_df) <- parsing_pl[,1]

# Updating Mash input

parsing_mash <- read.table('../Files/Plasmid_annotation/mash_distances.txt')
strains <- parsing_mash[,1]
mash_df <- parsing_mash[,c(2:306)]
  
strains <- gsub('.fasta.gz','',strains)
  
colnames(mash_df) <- strains
rownames(mash_df) <- strains
```

## Clustering complete plasmid sequences

``` r
### Hierarchical clustering to compare complete pl. sequences
hc_mash <- hclust(as.dist(mash_df), method = 'ward.D2', members = NULL)
tree_hc_mash <- as.phylo(hc_mash)
```

## Complete plasmids tree + Plasmid annotation + Metadata

``` r
## Highlighting particular nodes important for the stories explained in the paper

pl_colors <- data.frame(label = parsing_pl[,1],
                        color_pl = rep('white',nrow(parsing_pl)))

pl_colors$color_pl <- ifelse(pl_colors$label %in% c("E8691_2","E4457_2","E8172_2","E4438_2","E8481_2","E4402_2","E8927_2","E4413_2"),"Pop 7 (Dogs)",
                             ifelse(pl_colors$label %in% c("E4456_2","E8172_2","E7199_2"),"Pop 4 (Hosp.)",'non-determined'))
      

pl_colors <- subset(pl_colors, pl_colors$color_pl != 'non-determined')

# Generating a dataframe in which we combine plasmid annotation and metadata information

metadata_isolation <- merge(plasmid_stats, metadata, by = 'Strain')



tree_strains_info <- data.frame(label = as.character(metadata_isolation$Plasmid),
           Isolation = as.character(metadata_isolation$isolation.source))

# Adding isolation source information in the plot 

tree_strains_info$Isolation <- factor(tree_strains_info$Isolation, levels = c('Hospitalized patient','Non-hospitalized person','Dog','Pig','Chicken'))

plot_isolation <- facet_plot(ggtree(tree_hc_mash, layout = 'rectangular'), panel = 'Source', data = tree_strains_info, geom=geom_tile, aes(x=as.numeric(as.factor(Isolation)), fill = Isolation)) + theme_tree2(legend.position = 'bottom')   

# Adding Rip groups to the plot

tree_rep_info <- data.frame(label = as.character(metadata_pl$Plasmid),
           Rep = metadata_pl$all_replicons,
           Resistance = metadata_pl$draft_resistance,
           Size = metadata_pl$Length/1e3)

tree_rep_parsing <- tree_rep_info %>% separate(Rep, c("A", "B","C","D"), sep = ',')
tree_rep_parsing$number <- NULL

tree_replicons <- tree_rep_parsing %>%
  gather(key = label, value = value )

first_replicon <- data.frame(label = tree_rep_parsing$label,
                             rep = tree_rep_parsing$A)

second_replicon <- data.frame(label = tree_rep_parsing$label,
                             rep = tree_rep_parsing$B)
c
```

    ## function (...)  .Primitive("c")

``` r
third_replicon <- data.frame(label = tree_rep_parsing$label,
                             rep = tree_rep_parsing$C)

fourth_replicon <- data.frame(label = tree_rep_parsing$label,
                              rep = tree_rep_parsing$D)

replicons <- rbind(first_replicon, second_replicon)
replicons <- rbind(replicons, third_replicon)
replicons <- rbind(replicons, fourth_replicon)

replicons <- na.omit(replicons)

replicons$number <- ifelse(replicons$rep == 'RepA_N', 1,
                   ifelse(replicons$rep == 'Inc18', 2,
                   ifelse(replicons$rep == 'Rep_trans',3,
                   ifelse(replicons$rep == 'Rep_1',4,
                   ifelse(replicons$rep == 'Rep_3',5,NA)))))


tree_heatmap <- facet_plot(plot_isolation, panel = 'RIP', data = replicons, geom=geom_tile, aes(x=number), fill = 'grey')       

plot_pl_features <- facet_plot(tree_heatmap, panel = 'Plasmid size', data = tree_rep_info, geom=geom_segment, aes(x=0, xend=Size, y=y, yend=y), color = 'black',stat = 'identity') + theme_tree2(legend.position = 'bottom') + scale_fill_manual(values = c("#dc143c", "#0000ff", "#9acd32", "#ff69b4","#b8860b"))

plot_pl_features
```

![](analysis_files/figure-gfm/unnamed-chunk-13-1.png)<!-- -->

# mlplasmids prediction

## Distribution of plasmid- and chromosome-posterior probabilities

``` r
# Loading mlplasmids prediction for all short-read contigs (Illumina) present in our collection 

prediction_contigs <- read.csv('../Files/mlplasmids_prediction/prediction_svm.tsv',sep='\t', header = FALSE)
colnames(prediction_contigs) <- c('Prob_Chr','Prob_Plasmid','Prediction','Contig','Strain','Length')

prediction_contigs$Strain <- substr(prediction_contigs$Contig, start=1, stop=6)
prediction_contigs$Strain <- gsub('_','',prediction_contigs$Strain)

prediction_contigs <- merge(prediction_contigs,metadata,by='Strain')

plot_prediction <- ggplot(prediction_contigs, aes(x=as.factor(Prediction),y=Prob_Plasmid,colour=as.factor(Prediction)))

plot_mlplasmids <- ggviolin(data = prediction_contigs, x = 'Prediction', y = 'Prob_Plasmid', color = 'Prediction', palette = 'jco', ylab = 'Plasmid class posterior probabilities', xlab = '')
```

## Report

``` r
predicted_plasmid_contigs <- subset(prediction_contigs,prediction_contigs$Prediction=='Plasmid') 
predicted_chr_contigs <- subset(prediction_contigs,prediction_contigs$Prediction=='Chromosome')

## Number of predicted plasmid contigs

nrow(predicted_plasmid_contigs)
```

    ## [1] 94485

``` r
## Number of predicted chromosomal contigs

nrow(predicted_chr_contigs)
```

    ## [1] 194884

``` r
# Summary probabilities associated to each class 

prediction_contigs %>% 
  group_by(Prediction) %>%
  summarize(mean(Prob_Chr),
            mean(Prob_Plasmid))
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Prediction"],"name":[1],"type":["fctr"],"align":["left"]},{"label":["mean(Prob_Chr)"],"name":[2],"type":["dbl"],"align":["right"]},{"label":["mean(Prob_Plasmid)"],"name":[3],"type":["dbl"],"align":["right"]}],"data":[{"1":"Chromosome","2":"0.94932889","3":"0.05067111"},{"1":"Plasmid","2":"0.09127413","3":"0.90872587"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

## Filtering out uncertain contigs

``` r
# Applying a threshold of 0.7

predicted_plasmid_contigs <- subset(predicted_plasmid_contigs, predicted_plasmid_contigs$Prob_Plasmid > 0.7 & predicted_plasmid_contigs$Length > 1000)
predicted_chr_contigs <- subset(predicted_chr_contigs, predicted_chr_contigs$Prob_Chr > 0.7 & predicted_chr_contigs$Length > 1000)

# Average number of plasmid-derived contigs per strain
number_plasmid_contigs <- aggregate(Length ~ Strain , data = predicted_plasmid_contigs, length)
nrow(predicted_plasmid_contigs)
```

    ## [1] 68363

``` r
round(mean(number_plasmid_contigs$Length),1)
```

    ## [1] 41.7

``` r
# Average number of chromosome-derived contigs per strain
number_chr_contigs <- aggregate(Length ~ Strain , data = predicted_chr_contigs, length)
nrow(predicted_chr_contigs)
```

    ## [1] 165200

``` r
round(mean(number_chr_contigs$Length))
```

    ## [1] 100

``` r
# Average number of bp predicted as plasmid per strain
cumulative_plasmid_length <- aggregate(Length ~ Strain , data = predicted_plasmid_contigs, sum)
mean(cumulative_plasmid_length$Length)
```

    ## [1] 232988.8

``` r
# Average number of bp predicted as chromosome per strain
cumulative_chr_length <- aggregate(Length ~ Strain , data = predicted_chr_contigs, sum)
mean(cumulative_chr_length$Length)
```

    ## [1] 2610023

# E. faecium host adaptation

## Genome size differences

``` r
# Merging in a single dataframe all predicted contigs

overall_prediction <- merge(number_plasmid_contigs,number_chr_contigs,by='Strain')
overall_prediction <- merge(overall_prediction,cumulative_plasmid_length,by='Strain')
overall_prediction <- merge(overall_prediction,cumulative_chr_length,by='Strain')
```

    ## Warning in merge.data.frame(overall_prediction, cumulative_chr_length, by =
    ## "Strain"): column names 'Length.x', 'Length.y' are duplicated in the result

``` r
colnames(overall_prediction) <- c('Strain','Number_plasmid_contigs','Number_chr_contigs','Length_plasmid_contigs','Length_chr_contigs')
overall_prediction$Percentage_plasmid <- overall_prediction$Length_plasmid_contigs/overall_prediction$Length_chr_contigs

# Adding source of isolation

source_info <- subset(metadata, select=c("Strain", "source_characteristic"))
overall_prediction <- merge(overall_prediction,source_info,by='Strain')

# Graph showing genome size differences

visualization_prediction <- overall_prediction[!overall_prediction$source_characteristic %in% c('Human','Food','Wild reservoir'),] 

visualization_prediction$Length_plasmid_contigs <- visualization_prediction$Length_plasmid_contigs/10e2 # Converting to kbp
visualization_prediction$Length_chr_contigs <- visualization_prediction$Length_chr_contigs/10e5 # Converting to Mbp

## Plasmid size differences

plot_pub_pl <- ggboxplot(data = visualization_prediction, x = 'source_characteristic', y = 'Length_plasmid_contigs', color = 'source_characteristic', palette = c("#dc143c", '#0000ff', "#9acd32", "#ff69b4", "#b8860b"), order = c('Hospitalized patient','Non-hospitalized person','Pet','Pig','Poultry'),add = 'jitter', ylab = 'Cumulative plasmid length (kbp)', xlab = '', legend = 'none') + rotate_x_text(45) +  geom_hline(yintercept = mean(visualization_prediction$Length_plasmid_contigs), linetype = 2) + theme(axis.text.x = NULL)

plot_pub_pl <- plot_pub_pl + stat_compare_means(label.y = 550) +
  stat_compare_means(ref.group = "Hospitalized patient", label = "p.signif", label.y = 500)

## Chromosome size differences
          
plot_pub_chr <- ggboxplot(data = visualization_prediction, x = 'source_characteristic', y = 'Length_chr_contigs', color = 'source_characteristic', palette = c("#dc143c", '#0000ff', "#9acd32", "#ff69b4", "#b8860b"), order = c('Hospitalized patient','Non-hospitalized person','Pet','Pig','Poultry'),add = 'jitter', ylab = 'Cumulative chromosome length (Mbp)', xlab = '', legend = 'none') + rotate_x_text(45) +  geom_hline(yintercept = mean(visualization_prediction$Length_chr_contigs), linetype = 2) + theme(axis.text.x = NULL)

plot_pub_chr <- plot_pub_chr + stat_compare_means(label.y = 3.1) +
  stat_compare_means(ref.group = "Hospitalized patient", label = "p.signif", label.y = 2.9)

plot_diff_isolation <- plot_grid(plot_pub_pl, plot_pub_chr,nrow = 2, ncol = 1)
plot_diff_isolation
```

![](analysis_files/figure-gfm/unnamed-chunk-17-1.png)<!-- -->

``` r
plot_pub_pl
```

![](analysis_files/figure-gfm/unnamed-chunk-17-2.png)<!-- -->

# Mash analysis of plasmid-predicted sequences

## Mash distances from plasmid-derived contigs (k = 21, s = 1,000)

``` r
plasmid_mash <- read.table('../Files/Phylogenies/plasmid_predicted/mash_distances.txt')
strains <- plasmid_mash[,1]
mash_df <- plasmid_mash[,c(2:1640)]
  
strains <- gsub('.fasta.gz','',strains)
  
colnames(mash_df) <- strains
rownames(mash_df) <- strains

dist_mash <- as.dist(mash_df)

plot_mash <- as.data.frame(mash_df) %>%
  gather(key = isolate, value = mash_distance)
```

## Fitting a parametric distribution to our empirical data

``` r
non_zero_indexes <- which(plot_mash$mash_distance!=0)
non_zero_mash_dist <- plot_mash$mash_distance[non_zero_indexes]

fit.weibull <- fitdist(non_zero_mash_dist, distr = "weibull", method = "mle")
fit.gamma <- fitdist(non_zero_mash_dist, distr = "gamma", method = "mle")
fit.lnorm <- fitdist(non_zero_mash_dist, distr = "lnorm", method = "mle")

# This code was run in an HPC environment, we saved the results in the object: 'distribution_models.Rdata'
```

``` r
load('../Files/Phylogenies/plasmid_predicted/distribution_models.Rdata')

summary(fit.weibull)
```

    ## Fitting of the distribution ' weibull ' by maximum likelihood 
    ## Parameters : 
    ##         estimate   Std. Error
    ## shape 1.26600013 4.507704e-04
    ## scale 0.07134556 3.641882e-05
    ## Loglikelihood:  4811113   AIC:  -9622222   BIC:  -9622197 
    ## Correlation matrix:
    ##           shape     scale
    ## shape 1.0000000 0.3304341
    ## scale 0.3304341 1.0000000

``` r
summary(fit.gamma)
```

    ## Fitting of the distribution ' gamma ' by maximum likelihood 
    ## Parameters : 
    ##        estimate  Std. Error
    ## shape  2.344073 0.001896932
    ## rate  35.870449 0.032358751
    ## Loglikelihood:  5082065   AIC:  -10164126   BIC:  -10164101 
    ## Correlation matrix:
    ##           shape      rate
    ## shape 1.0000000 0.8970691
    ## rate  0.8970691 1.0000000

``` r
summary(fit.lnorm)
```

    ## Fitting of the distribution ' lnorm ' by maximum likelihood 
    ## Parameters : 
    ##           estimate   Std. Error
    ## meanlog -2.9562306 0.0004227089
    ## sdlog    0.6926065 0.0002988975
    ## Loglikelihood:  5113176   AIC:  -10226347   BIC:  -10226322 
    ## Correlation matrix:
    ##               meanlog         sdlog
    ## meanlog  1.000000e+00 -3.235911e-10
    ## sdlog   -3.235911e-10  1.000000e+00

``` r
# We chose the Gamma distribution to fit our data

denscomp(ft = fit.gamma)
```

![](analysis_files/figure-gfm/unnamed-chunk-20-1.png)<!-- -->

``` r
cdfcomp(ft = fit.gamma)
```

![](analysis_files/figure-gfm/unnamed-chunk-20-2.png)<!-- -->

``` r
# We can for example know what is the probability of observing a mash distance of less than 0.02 in our distance set

pgamma(q = 0.02, shape = fit.gamma$estimate[1], rate = fit.gamma$estimate[2], lower.tail = TRUE)
```

    ## [1] 0.1001025

``` r
# But most important is that we can calculate the quantiles of our data. In this way we can have a feeling about the distance thresholds

first_quantile <- qgamma(p = 0.25, shape = fit.gamma$estimate[1], rate = fit.gamma$estimate[2], lower.tail = TRUE)
second_quantile <- qgamma(p = 0.5, shape = fit.gamma$estimate[1], rate = fit.gamma$estimate[2], lower.tail = TRUE)
third_quantile <- qgamma(p = 0.75, shape = fit.gamma$estimate[1], rate = fit.gamma$estimate[2], lower.tail = TRUE)

# We can also define a threshold to really see which isolates show a very low distance

similarity_isolates <- qgamma(p = 0.01, shape = fit.gamma$estimate[1], rate = fit.gamma$estimate[2], lower.tail = TRUE)

# We can also exclude some strains to avoid affecting the clustering process
# Here we are considering what is the mash distance in which our 90% of isolates are located. Most of them could be errors which we are not interested in considering. 

dist_exclusion <- qgamma(p = 0.9, shape = fit.gamma$estimate[1], rate = fit.gamma$estimate[2], lower.tail = TRUE)

# We include the strains with a distance lower than the distance of exclusion

strains_to_include <- names(which(lapply(mash_df, mean) < dist_exclusion))

polished_mash_df <- mash_df[ colnames(mash_df) %in% strains_to_include, rownames(mash_df) %in% strains_to_include]

# This distance matrix is the one that we consider to calculate the clusters present in our data. 

dim(polished_mash_df)
```

    ## [1] 1607 1607

``` r
# Looking at the differences before/after excluding the isolates

raw_mash_bionj <- midpoint(bionj(as.matrix(mash_df)))
raw_ggtree_bionj <- ggtree(raw_mash_bionj, layout = 'rectangular')

polish_mash_bionj <- midpoint(bionj(as.matrix(polished_mash_df)))
polished_ggtree_bionj <- ggtree(polish_mash_bionj, layout = 'rectangular')

comparison_before_after_exclusion <- plot_grid(raw_ggtree_bionj, polished_ggtree_bionj, ncol = 2, labels = c('A','B'))
comparison_before_after_exclusion
```

![](analysis_files/figure-gfm/unnamed-chunk-21-1.png)<!-- -->

## Plasmid populations

### Defining initial number of clusters

``` r
nbclust_ward_silhouette <- NbClust(data = NULL, diss = dist_matrix, distance = NULL, min.nc = 2, max.nc = 100, method = 'ward.D2', index = 'silhouette')

viz_nbclust_silhouette <- data.frame(Number_Clusters = as.factor(as.numeric(names(nbclust_ward_silhouette$All.index))),
                                     Silhouette_Index = nbclust_ward_silhouette$All.index)

silhouette_plot <- ggplot(viz_nbclust_silhouette, aes(x = Number_Clusters, y = Silhouette_Index, group = 1)) + geom_point() + geom_line() + geom_vline(xintercept = 25, linetype="dotted", color = "blue", size=1.0 ) +  labs(x = "Number of clusters", y = "Average silhouette index") + theme_classic() + theme(axis.text.x = element_text(angle = -90, hjust = 0.5)) 
```

``` r
number_clusters <- 26

ward_clustering <- factoextra::hcut(x = as.dist(polished_mash_df), isdiss = TRUE, k = number_clusters, hc_metric = NULL, hc_func = 'hclust', hc_method = 'ward.D2')

ward_clustering$silinfo$clus.avg.widths
```

    ##  [1]  0.74995756  0.07234694 -0.01472431  0.08973794  0.05900421
    ##  [6] -0.07456315  0.33207511  0.81867101  0.05955377  0.52976426
    ## [11]  0.22459914  0.50161508  0.44379771  0.40599387  0.59960823
    ## [16]  0.24299331  0.21642835  0.39006540  0.31409701  0.39631684
    ## [21]  0.59228383  0.22214833  0.64427824  0.62465944  0.89717461
    ## [26]  0.66378820

``` r
# Focusing on large clusters with a good silhouette average width 

silhouette_results <- fviz_silhouette(ward_clustering)
```

    ##    cluster size ave.sil.width
    ## 1        1   99          0.75
    ## 2        2   58          0.07
    ## 3        3   37         -0.01
    ## 4        4   56          0.09
    ## 5        5   23          0.06
    ## 6        6   64         -0.07
    ## 7        7   16          0.33
    ## 8        8   52          0.82
    ## 9        9    6          0.06
    ## 10      10  125          0.53
    ## 11      11   27          0.22
    ## 12      12   50          0.50
    ## 13      13   19          0.44
    ## 14      14  139          0.41
    ## 15      15  110          0.60
    ## 16      16   40          0.24
    ## 17      17   99          0.22
    ## 18      18   21          0.39
    ## 19      19   83          0.31
    ## 20      20    8          0.40
    ## 21      21    8          0.59
    ## 22      22   32          0.22
    ## 23      23  160          0.64
    ## 24      24  214          0.62
    ## 25      25   53          0.90
    ## 26      26    8          0.66

``` r
cluster_df <- silhouette_results$data

stats_cluster <- cluster_df %>%
  group_by(cluster) %>%
  summarize(avg_sil_width = mean(sil_width), size=n())

potential_pl_pop <- stats_cluster %>%
  filter(avg_sil_width > 0.3) %>%
  filter(size > 50) 

potential_pl_pop$avg_sil_width <- round(potential_pl_pop$avg_sil_width, 2)
```

### Definition of plasmid populations and metadata inclusion

``` r
# Defining 26 colors

qual_col_pals <- brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector <- unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))

col_vector <- sample(col_vector, number_clusters)

strains <- ward_clustering$labels

clusters_strains <- data.frame(Strain = names(ward_clustering$cluster),
                             Population = ward_clustering$cluster)

clusters_strains$Defined <- ifelse(clusters_strains$Population %in% potential_pl_pop$cluster, 2, 1)

#clusters_strains$Defined <- ifelse(clusters_strains$Population %in% c(23),2,1)

clusters_strains_metadata <- merge(metadata, clusters_strains, by = 'Strain')


metadata_colors <- ifelse(clusters_strains_metadata$isolation.source == 'Hospitalized patient', '#dc143c',
                   ifelse(clusters_strains_metadata$isolation.source == 'Non-hospitalized person', '#0000ff',
                   ifelse(clusters_strains_metadata$isolation.source == 'Pig','#ff69b4',
                   ifelse(clusters_strains_metadata$isolation.source == 'Chicken','#b8860b',
                   ifelse(clusters_strains_metadata$isolation.source == 'Dog','#9acd32','black')))))

colors_populations <- col_vector[clusters_strains$Population]

col_selection_vector <- c('white','blue')

colors_pop_selection <- col_selection_vector[clusters_strains$Defined]

non_selected_pop <- which(clusters_strains$Defined == 1)

colors_populations[non_selected_pop] <- 'white' 


dendrogram_hc_mash <- as.phylo(ward_clustering)
dendrogram_tree <- ggtree(dendrogram_hc_mash) + coord_flip() + scale_x_reverse() + scale_y_reverse()

sel <- data.frame(Strain = clusters_strains$Strain,
                  Population = clusters_strains$Population)
sel$Strain <- as.character(sel$Strain)

rownames(sel) <- sel$Strain
sel$Strain <- NULL

viz_colors <- col_vector
viz_colors[setdiff(as.numeric(clusters_strains$Population),potential_pl_pop$cluster)] <- 'white'

# Heatmap visualization

heatmap.2(as.matrix(polished_mash_df),
          Colv=as.dendrogram(ward_clustering),
          Rowv=as.dendrogram(ward_clustering),  # Plot histogram of data and colour key
          trace="none",               # Turn of trace lines from heat map
          col = c('black','red','orange','yellow','white'), 
          ColSideColors = colors_populations,
          labRow = '',
          labCol = '',
          RowSideColors = metadata_colors,
          breaks = c(0,similarity_isolates,first_quantile,second_quantile,third_quantile,1.0),
          cexRow=0.5,cexCol=0.5,
          key = FALSE, title(main = 'Hierachical clustering'))
```

![](analysis_files/figure-gfm/unnamed-chunk-24-1.png)<!-- -->

### Exploring the plasmid subpopulations

``` r
# The order of the plasmid subpopulations in the heatmap is not from 1 to 26 

strains_position <- order.dendrogram(x = as.dendrogram(ward_clustering))

appearance_pl_pop <- unique(clusters_strains$Population[strains_position])

# Looking at particular plasmid subpopulations 

gather_mash <- polished_mash_df %>%
  gather(key = isolate, value = mash_distance)


pl_pop_metadata <- subset(clusters_strains_metadata, clusters_strains_metadata$isolation.source %in% c('Hospitalized patient','Non-hospitalized person','Pig','Chicken','Dog'))

# We create a for loop to check for each category (Hospitalized, Dog...) if there is a significant association with a particular plasmid population defined before 

results_association <- NULL

for(cluster in levels(as.factor(pl_pop_metadata$Population)))
{
    df_cluster <- subset(pl_pop_metadata, pl_pop_metadata$Population == cluster)
    df_other_clusters <- subset(pl_pop_metadata, pl_pop_metadata$Population != cluster)
    
    df_counts_cluster <- as.data.frame(table(df_cluster$isolation.source))
    df_counts_other_clusters <- as.data.frame(table(df_other_clusters$isolation.source))
    
    for(category in df_counts_cluster$Var1)
    {
      particular_cluster_cat <- subset(df_counts_cluster,df_counts_cluster$Var1 == category)
      particular_cluster_other_cat <- subset(df_counts_cluster,df_counts_cluster$Var1 != category)
      sumatory_cat <- sum(as.numeric(as.character(particular_cluster_cat$Freq)))
      sumatory_other_cat <- sum(as.numeric(as.character(particular_cluster_other_cat$Freq)))
      
      vector_cluster <- c(sumatory_cat, sumatory_other_cat)
      
      dif_clusters_cat <- subset(df_counts_other_clusters,df_counts_other_clusters$Var1 == category)
      dif_clusters_dif_cat <- subset(df_counts_other_clusters,df_counts_other_clusters$Var1 != category)
      sumatory_dif_clusters_cat <- sum(as.numeric(as.character(dif_clusters_cat$Freq)))
      sumatory_dif_clusters_dif_cat <- sum(as.numeric(as.character(dif_clusters_dif_cat$Freq)))
      
      vector_other_clusters <- c(sumatory_dif_clusters_cat, sumatory_dif_clusters_dif_cat)
      
      contingency_table <<- rbind(vector_cluster, vector_other_clusters)
      colnames(contingency_table) <- c('Particular_source','Other_sources')
      rownames(contingency_table) <- c('Particular_Cluster','Other_clusters')
      
          
    result_fisher_test <- fisher.test(x=contingency_table,alternative="greater") # We perform a Fisher test using our contingency table
    
    naive_p_value <- as.numeric(as.character(result_fisher_test[1]))
    
    result <- c(cluster, category, naive_p_value)
    results_association <<- rbind(results_association,result)
    
    }
    
}

colnames(results_association) <- c('Plasmid_subpopulation','isolation_source','naive_pvalue')
results_association <- as.data.frame(results_association)
results_association$naive_pvalue <- as.numeric(as.character(results_association$naive_pvalue))

results_association$bh_correction <- p.adjust(results_association$naive_pvalue, method = 'BH', n = length(results_association$naive_pvalue)) # Correcting by Benjamini-Hochberg (BH)
results_association$bonferroni_correction <- p.adjust(results_association$naive_pvalue, method = 'bonferroni', n = length(results_association$naive_pvalue)) # Correcting by Bonferroni

clusters_association <- subset(results_association, results_association$bonferroni_correction < 0.05)

clusters_association
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["Plasmid_subpopulation"],"name":[1],"type":["fctr"],"align":["left"]},{"label":["isolation_source"],"name":[2],"type":["fctr"],"align":["left"]},{"label":["naive_pvalue"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["bh_correction"],"name":[4],"type":["dbl"],"align":["right"]},{"label":["bonferroni_correction"],"name":[5],"type":["dbl"],"align":["right"]}],"data":[{"1":"1","2":"Non-hospitalized person","3":"5.699149e-13","4":"2.659603e-12","5":"3.191523e-11","_rn_":"result.2"},{"1":"1","2":"Pig","3":"8.694903e-51","4":"1.623048e-49","5":"4.869145e-49","_rn_":"result.3"},{"1":"2","2":"Non-hospitalized person","3":"1.391328e-05","4":"4.328575e-05","5":"7.791436e-04","_rn_":"result.6"},{"1":"2","2":"Pig","3":"7.318863e-15","4":"3.725967e-14","5":"4.098563e-13","_rn_":"result.7"},{"1":"3","2":"Non-hospitalized person","3":"5.845780e-15","4":"3.273637e-14","5":"3.273637e-13","_rn_":"result.10"},{"1":"5","2":"Non-hospitalized person","3":"3.014968e-07","4":"1.055239e-06","5":"1.688382e-05","_rn_":"result.19"},{"1":"6","2":"Chicken","3":"6.846283e-45","4":"9.584796e-44","5":"3.833919e-43","_rn_":"result.21"},{"1":"8","2":"Chicken","3":"1.395764e-65","4":"3.908139e-64","5":"7.816279e-64","_rn_":"result.25"},{"1":"10","2":"Hospitalized patient","3":"6.740763e-18","4":"5.392610e-17","5":"3.774827e-16","_rn_":"result.31"},{"1":"12","2":"Hospitalized patient","3":"5.530827e-08","4":"2.064842e-07","5":"3.097263e-06","_rn_":"result.34"},{"1":"14","2":"Dog","3":"6.598142e-128","4":"3.694959e-126","5":"3.694959e-126","_rn_":"result.38"},{"1":"15","2":"Hospitalized patient","3":"1.007991e-15","4":"6.902510e-15","5":"5.644747e-14","_rn_":"result.42"},{"1":"16","2":"Hospitalized patient","3":"1.248407e-06","4":"4.112400e-06","5":"6.991080e-05","_rn_":"result.43"},{"1":"17","2":"Hospitalized patient","3":"1.109332e-15","4":"6.902510e-15","5":"6.212259e-14","_rn_":"result.44"},{"1":"18","2":"Hospitalized patient","3":"8.377203e-04","4":"2.233921e-03","5":"4.691234e-02","_rn_":"result.45"},{"1":"19","2":"Hospitalized patient","3":"1.797948e-11","4":"7.745009e-11","5":"1.006851e-09","_rn_":"result.46"},{"1":"22","2":"Hospitalized patient","3":"1.957091e-05","4":"5.768269e-05","5":"1.095971e-03","_rn_":"result.50"},{"1":"23","2":"Hospitalized patient","3":"1.650968e-25","4":"1.540903e-24","5":"9.245419e-24","_rn_":"result.51"},{"1":"24","2":"Hospitalized patient","3":"1.199782e-34","4":"1.343756e-33","5":"6.718781e-33","_rn_":"result.52"},{"1":"25","2":"Hospitalized patient","3":"1.374502e-08","4":"5.498008e-08","5":"7.697211e-07","_rn_":"result.53"},{"1":"26","2":"Dog","3":"7.490646e-04","4":"2.097381e-03","5":"4.194762e-02","_rn_":"result.54"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
potential_pop <- clusters_association[clusters_association$Plasmid_subpopulation %in% potential_pl_pop$cluster,]

# Let's take a look at the diversity that we can find in our plasmid subpopulations defined by hierarchical clustering 

# Let's do looking at BAPS groups (SC type in the article), using postBNGBAPS.3

pl_pop_baps <- pl_pop_metadata %>%
  group_by(Population) %>%
  count(postBNGBAPS.2)

pl_pop_baps$Population <- as.factor(as.character(pl_pop_baps$Population))
pl_pop_baps$postBNGBAPS.2 <- as.factor(as.character(pl_pop_baps$postBNGBAPS.2))
pl_pop_baps$n <- as.numeric(pl_pop_baps$n)

spread_pl_pop_baps <- pl_pop_baps %>%
  spread(key = postBNGBAPS.2, value = n, fill = 0)

spread_pl_pop_baps <- as.matrix(spread_pl_pop_baps)

rownames(spread_pl_pop_baps) <- spread_pl_pop_baps[,1]

spread_pl_pop_baps <- spread_pl_pop_baps[,c(2:ncol(spread_pl_pop_baps))]

matrix_spread_pl_pop_baps <- apply(spread_pl_pop_baps, 1,as.numeric)

stats_pl_pop_baps <- round(diverse::diversity(data = matrix_spread_pl_pop_baps, type = c('s'), q = 1, category_row = TRUE),2)

stats_pl_pop_baps$pl_subpopulation <- as.numeric(rownames(stats_pl_pop_baps))
stats_pl_pop_baps$color <- col_vector[stats_pl_pop_baps$pl_subpopulation]
stats_pl_pop_baps <- stats_pl_pop_baps[order(stats_pl_pop_baps$pl_subpopulation),]

selected_pl_pop <- stats_pl_pop_baps[stats_pl_pop_baps$pl_subpopulation %in% potential_pl_pop$cluster,]


ggplot(selected_pl_pop, aes(x = as.factor(selected_pl_pop$pl_subpopulation), y = selected_pl_pop$simpson.I, fill = as.factor(selected_pl_pop$pl_subpopulation))) + geom_col() + scale_fill_manual(values = selected_pl_pop$color) + 
    labs(x='Plasmid populations)',y='Simpson index',title = 'Are pl. pops driven by horiz/vertical transmission? postBNGBAPs.2 diversity') + theme(legend.position = 'none') + ylim(c(0,1)) 
```

![](analysis_files/figure-gfm/unnamed-chunk-25-1.png)<!-- -->

``` r
silhouette_values <- ward_clustering$silinfo$clus.avg.widths

stats_pl_pop_baps <- cbind(stats_pl_pop_baps, silhouette_values)

gather_stats_pop <- stats_pl_pop_baps %>%
  gather(key = Metric, value = Value, -pl_subpopulation, -color) %>%
  filter(Metric %in% c('simpson.I','silhouette_values'))

selected_gather_stats_pop <- gather_stats_pop[gather_stats_pop$pl_subpopulation %in% potential_pl_pop$cluster,]
selected_gather_stats_pop$Value <- round(selected_gather_stats_pop$Value, 2)

selected_gather_stats_pop$pl_subpopulation <- as.factor(selected_gather_stats_pop$pl_subpopulation)

selected_gather_stats_pop$pl_subpopulation <- ifelse(selected_gather_stats_pop$pl_subpopulation == 1, 1, 
       ifelse(selected_gather_stats_pop$pl_subpopulation == 8, 2, 
        ifelse(selected_gather_stats_pop$pl_subpopulation == 10, 3,
        ifelse(selected_gather_stats_pop$pl_subpopulation == 14, 4,
        ifelse(selected_gather_stats_pop$pl_subpopulation == 15, 5,
        ifelse(selected_gather_stats_pop$pl_subpopulation == 19, 6,
        ifelse(selected_gather_stats_pop$pl_subpopulation == 23, 7,
        ifelse(selected_gather_stats_pop$pl_subpopulation == 24, 8, 
        ifelse(selected_gather_stats_pop$pl_subpopulation == 25, 9, 0)))))))))


populations_plot <- ggplot(selected_gather_stats_pop, aes(x = as.factor(pl_subpopulation), y = Value, group = Metric, color = as.factor(pl_subpopulation), shape = Metric)) + geom_point(size = 5.0) + geom_hline(yintercept= ward_clustering$silinfo$avg.width, linetype="dashed", color = "red") + scale_color_manual(values = selected_gather_stats_pop$color) + theme_bw() + theme(legend.position = 'top', text = element_text(size=14)) + labs(x = "Plasmidome population", y = "Value", color = 'Plasmidome population') + scale_shape(labels = c("Silhouette value", "Simpson index"))  + scale_color_manual(values = c('#FBB4AE', '#FFF2AE', '#1B9E77', '#BEAED4','#FED9A6','#1F78B4','#E5D8BD','#B3CDE3','#8DA0CB')) 
```

    ## Scale for 'colour' is already present. Adding another scale for
    ## 'colour', which will replace the existing scale.

``` r
populations_plot
```

![](analysis_files/figure-gfm/unnamed-chunk-25-2.png)<!-- -->

## Exploring the core- and soft-core plasmidome of each population

``` r
# Core- and soft-core genes present in all the isolates with plasmidome prediction associated

genes_all_pop <- read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/gene_presence_absence.csv')
soft_and_core_all_pop <- subset(genes_all_pop, genes_all_pop$No..isolates > (1607/100*95))

# Reading the annotation that we obtained in eggnog 

eggnog_all_pop <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/eggnog_cleaned.tab', sep = '\t')
colnames(eggnog_all_pop) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')

# Reading the file generated by Roary to match the name of the group_OG with the gene name 

user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_all_pop <- merge(gene_groups, eggnog_all_pop, by = 'query_name')

soft_core_eggnog_all_pop <- merge(eggnog_genes_all_pop, soft_and_core_all_pop, by = 'Gene')

# We have done this analysis population by population 

genes_pop1 <- read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population1/gene_presence_absence.csv')
soft_and_core_pop1 <- subset(genes_pop1, genes_pop1$No..isolates > (99/100*95))

genes_pop2 <-  read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population8/gene_presence_absence.csv')
soft_and_core_pop2 <- subset(genes_pop2, genes_pop2$No..isolates > (52/100*95))

genes_pop3 <-  read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population10/gene_presence_absence.csv')
soft_and_core_pop3 <- subset(genes_pop3, genes_pop3$No..isolates > (124/100*95))

genes_pop4 <-  read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population14/gene_presence_absence.csv')
soft_and_core_pop4 <- subset(genes_pop4, genes_pop4$No..isolates > (133/100*95))

genes_pop5 <-  read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population15/gene_presence_absence.csv')
soft_and_core_pop5 <- subset(genes_pop5, genes_pop5$No..isolates > (110/100*95))

genes_pop6 <-  read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population19/gene_presence_absence.csv')
soft_and_core_pop6 <- subset(genes_pop6, genes_pop6$No..isolates > (82/100*95))

genes_pop7 <-  read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population23/gene_presence_absence.csv')
soft_and_core_pop7 <- subset(genes_pop7, genes_pop7$No..isolates > (160/100*95))

genes_pop8 <-  read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population24/gene_presence_absence.csv')
soft_and_core_pop8 <- subset(genes_pop8, genes_pop8$No..isolates > (214/100*95))

genes_pop9 <-  read.csv(file = '../Files/mlplasmids_prediction/Roary_mash/population25/gene_presence_absence.csv')
soft_and_core_pop9 <- subset(genes_pop9, genes_pop9$No..isolates > (53/100*95))

# Eggnog annotation 

## Population 1

eggnog_pop1 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population1/eggnog_cleaned.tab', sep = '\t')
```

    ## Warning in scan(file = file, what = what, sep = sep, quote = quote, dec =
    ## dec, : EOF within quoted string

``` r
colnames(eggnog_pop1) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')

user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population1/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop1 <- merge(gene_groups, eggnog_pop1, by = 'query_name')


## Population 2


eggnog_pop2 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population8/eggnog_cleaned.tab', sep = '\t')
colnames(eggnog_pop2) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')


user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population8/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop2 <- merge(gene_groups, eggnog_pop2, by = 'query_name')


## Population 3

eggnog_pop3 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population10/eggnog_cleaned.tab', sep = '\t')
```

    ## Warning in scan(file = file, what = what, sep = sep, quote = quote, dec =
    ## dec, : EOF within quoted string

``` r
colnames(eggnog_pop3) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')

user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population10/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop3 <- merge(gene_groups, eggnog_pop3, by = 'query_name')

## Population 4

eggnog_pop4 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population14/eggnog_cleaned.tab', sep = '\t')
```

    ## Warning in scan(file = file, what = what, sep = sep, quote = quote, dec =
    ## dec, : EOF within quoted string

``` r
colnames(eggnog_pop4) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')

user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population14/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop4 <- merge(gene_groups, eggnog_pop4, by = 'query_name')

## Population 5

eggnog_pop5 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population15/eggnog_cleaned.tab', sep = '\t')
```

    ## Warning in scan(file = file, what = what, sep = sep, quote = quote, dec =
    ## dec, : EOF within quoted string

``` r
colnames(eggnog_pop5) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')


user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population15/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop5 <- merge(gene_groups, eggnog_pop5, by = 'query_name')



## Population 6


eggnog_pop6 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population19/eggnog_cleaned.tab', sep = '\t')
```

    ## Warning in scan(file = file, what = what, sep = sep, quote = quote, dec =
    ## dec, : EOF within quoted string

``` r
colnames(eggnog_pop6) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')

user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population19/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop6 <- merge(gene_groups, eggnog_pop6, by = 'query_name')


## Population 7

eggnog_pop7 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population23/eggnog_cleaned.tab', sep = '\t')
colnames(eggnog_pop7) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')

user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population23/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop7 <- merge(gene_groups, eggnog_pop7, by = 'query_name')




## Population 8

eggnog_pop8 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population24/eggnog_cleaned.tab', sep = '\t')
```

    ## Warning in scan(file = file, what = what, sep = sep, quote = quote, dec =
    ## dec, : EOF within quoted string

``` r
colnames(eggnog_pop8) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')

user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population24/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop8 <- merge(gene_groups, eggnog_pop8, by = 'query_name')


## Population 9 


eggnog_pop9 <- read.table(file = '../Files/mlplasmids_prediction/Roary_mash/population25/eggnog_cleaned.tab', sep = '\t')
colnames(eggnog_pop9) <- c('query_name','seed_ortholog','seed_evalue','seed_score','predicted_gene_name','GO_terms','KEGG_KOs','BiGG_reactions','Annotation_tax_scope','OGs','BestOG','COG_cat','eggNOG_annotation')


user_file <- readDNAStringSet(filepath = '../Files/mlplasmids_prediction/Roary_mash/population25/pan_genome_reference.fa', format="fasta")
gene_names_pop <- names(user_file) # Header of the nodes

gene_groups <- data.frame(query_name = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,1],
           Gene = str_split_fixed(gene_names_pop, pattern = ' ', n = 2)[,2])

eggnog_genes_pop9 <- merge(gene_groups, eggnog_pop9, by = 'query_name')


soft_core_eggnog_pop1 <- merge(eggnog_genes_pop1, soft_and_core_pop1, by = 'Gene')
soft_core_eggnog_pop2 <- merge(eggnog_genes_pop2, soft_and_core_pop2, by = 'Gene')
soft_core_eggnog_pop3 <- merge(eggnog_genes_pop3, soft_and_core_pop3, by = 'Gene')
soft_core_eggnog_pop4 <- merge(eggnog_genes_pop4, soft_and_core_pop4, by = 'Gene')
soft_core_eggnog_pop5 <- merge(eggnog_genes_pop5, soft_and_core_pop5, by = 'Gene')
soft_core_eggnog_pop6 <- merge(eggnog_genes_pop6, soft_and_core_pop6, by = 'Gene')
soft_core_eggnog_pop7 <- merge(eggnog_genes_pop7, soft_and_core_pop7, by = 'Gene')
soft_core_eggnog_pop8 <- merge(eggnog_genes_pop8, soft_and_core_pop8, by = 'Gene')
soft_core_eggnog_pop9 <- merge(eggnog_genes_pop9, soft_and_core_pop9, by = 'Gene')


pop1 <- as.data.frame(soft_core_eggnog_pop1 %>%
  group_by(soft_core_eggnog_pop1$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population1')))

colnames(pop1) <- c('COG','Count','Population')

pop2 <- as.data.frame(soft_core_eggnog_pop2 %>%
  group_by(soft_core_eggnog_pop2$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population2')))

colnames(pop2) <- c('COG','Count','Population')

pop3 <- as.data.frame(soft_core_eggnog_pop3 %>%
  group_by(soft_core_eggnog_pop3$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population3')))

colnames(pop3) <- c('COG','Count','Population')

pop4 <- as.data.frame(soft_core_eggnog_pop4 %>%
  group_by(soft_core_eggnog_pop4$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population4')))

colnames(pop4) <- c('COG','Count','Population')

pop5 <- as.data.frame(soft_core_eggnog_pop5 %>%
  group_by(soft_core_eggnog_pop5$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population5')))

colnames(pop5) <- c('COG','Count','Population')

pop6 <- as.data.frame(soft_core_eggnog_pop6 %>%
  group_by(soft_core_eggnog_pop6$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population6')))

colnames(pop6) <- c('COG','Count','Population')

pop7 <- as.data.frame(soft_core_eggnog_pop7 %>%
  group_by(soft_core_eggnog_pop7$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population7')))

colnames(pop7) <- c('COG','Count','Population')


pop8 <- as.data.frame(soft_core_eggnog_pop8 %>%
  group_by(soft_core_eggnog_pop8$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population8')))

colnames(pop8) <- c('COG','Count','Population')

pop9 <- as.data.frame(soft_core_eggnog_pop9 %>%
  group_by(soft_core_eggnog_pop9$COG_cat) %>%
  count() %>%
  mutate(Population = rep('Population9')))

colnames(pop9) <- c('COG','Count','Population')

populations <- NULL

populations <- rbind(populations, pop1)
populations <- rbind(populations, pop2)
populations <- rbind(populations, pop3)
populations <- rbind(populations, pop4)
populations <- rbind(populations, pop5)
populations <- rbind(populations, pop6)
populations <- rbind(populations, pop7)
populations <- rbind(populations, pop8)
populations <- rbind(populations, pop9)

qual_col_pals <- brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector <- unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))

ggplot(populations, aes(x = COG, y = Count, group = as.factor(COG), fill = as.factor(COG))) + geom_bar(stat="identity") + scale_fill_brewer(palette="Paired") + theme_bw() + theme(axis.text.x = element_text(angle = 45, hjust = 1)) + facet_wrap(.~ Population, nrow = 5, ncol = 2) + labs(x = 'COG group', y = 'Core and soft-core genes annotation', fill = 'COG category') + scale_fill_manual(values = col_vector)
```

    ## Scale for 'fill' is already present. Adding another scale for 'fill',
    ## which will replace the existing scale.

![](analysis_files/figure-gfm/unnamed-chunk-26-1.png)<!-- -->

``` r
average_1 <- nrow(soft_core_eggnog_pop1)/nrow(soft_and_core_pop1)
average_2 <- nrow(soft_core_eggnog_pop2)/nrow(soft_and_core_pop2)
average_3 <- nrow(soft_core_eggnog_pop3)/nrow(soft_and_core_pop3)
average_4 <- nrow(soft_core_eggnog_pop4)/nrow(soft_and_core_pop4)
average_5 <- nrow(soft_core_eggnog_pop5)/nrow(soft_and_core_pop5)
average_6 <- nrow(soft_core_eggnog_pop6)/nrow(soft_and_core_pop6)
average_7 <- nrow(soft_core_eggnog_pop7)/nrow(soft_and_core_pop7)
average_8 <- nrow(soft_core_eggnog_pop8)/nrow(soft_and_core_pop8)
average_9 <- nrow(soft_core_eggnog_pop9)/nrow(soft_and_core_pop9)

cog_average <- mean(c(average_1, average_2, average_3, average_4, average_5, average_6, average_7, average_8, average_9))


average_s_1 <- 18/nrow(soft_core_eggnog_pop1)
average_s_2 <- 9/nrow(soft_core_eggnog_pop2)
average_s_3 <- 13/nrow(soft_core_eggnog_pop3)
average_s_4 <- 8/nrow(soft_core_eggnog_pop4)
average_s_5 <- 25/nrow(soft_core_eggnog_pop5)
average_s_6 <- 24/nrow(soft_core_eggnog_pop6)
average_s_7 <- 12/nrow(soft_core_eggnog_pop7)
average_s_8 <- 20/nrow(soft_core_eggnog_pop8)
average_s_9 <- 34/nrow(soft_core_eggnog_pop9)


cog_s_average <- mean(c(average_s_1, average_s_2, average_s_3, average_s_4, average_s_5, average_s_6, average_s_7, average_s_8, average_s_9))

core_average <- mean(c(nrow(soft_and_core_pop1), nrow(soft_and_core_pop2), nrow(soft_and_core_pop3), nrow(soft_and_core_pop4), nrow(soft_and_core_pop5), nrow(soft_and_core_pop5), nrow(soft_and_core_pop6), nrow(soft_and_core_pop7), nrow(soft_and_core_pop8), nrow(soft_and_core_pop9)))
```

# CRISPR-cas and RM systems

## Crispr-systems

``` r
# Crispr systems

clade_A1 <- read_csv('../Files/Metadata/cladeA1_metadata.csv')
```

    ## Parsed with column specification:
    ## cols(
    ##   ID = col_character(),
    ##   CladeA1 = col_character(),
    ##   `Strain code` = col_character(),
    ##   year = col_double(),
    ##   `country of isolation` = col_character(),
    ##   City_Region = col_character(),
    ##   `isolation source` = col_character(),
    ##   `isolation site` = col_character(),
    ##   source_characteristic = col_character(),
    ##   Hospital = col_character(),
    ##   `Lat/Long` = col_character(),
    ##   MLST = col_double(),
    ##   SC = col_double(),
    ##   van_gene = col_character()
    ## )

    ## Warning: 8 parsing failures.
    ##  row  col               expected actual                                     file
    ## 1341 year no trailing characters  -2000 '../Files/Metadata/cladeA1_metadata.csv'
    ## 1342 year no trailing characters  -2000 '../Files/Metadata/cladeA1_metadata.csv'
    ## 1343 year no trailing characters  -2000 '../Files/Metadata/cladeA1_metadata.csv'
    ## 1346 year no trailing characters  -2000 '../Files/Metadata/cladeA1_metadata.csv'
    ## 1372 year no trailing characters  -2000 '../Files/Metadata/cladeA1_metadata.csv'
    ## .... .... ...................... ...... ........................................
    ## See problems(...) for more details.

``` r
colnames(clade_A1)[1] <- 'Strain'

crispr_systems <- read.table(file = '../Files/Crispr/crispr_detected.txt')
crispr_systems[,1] <- gsub(pattern = '.fasta', replacement = '', x = crispr_systems[,1])
colnames(crispr_systems) <- 'Strain'


val_crispr <- read.table(file = '../Files/Crispr/confirmed_crispr.txt')
colnames(val_crispr) <- 'Strain'


clade_A1$CladeA1 <- ifelse(clade_A1$CladeA1 == 'Yes', 'Clade_A1', 'Non_clade_A1')


clade_B_strains <- which(clade_A1$Strain %in% clade_B_metadata$ID)
clade_A1$CladeA1[clade_B_strains] <- 'Clade_B'


crispr_systems <- merge(crispr_systems, clade_A1, by = 'Strain')
val_crispr_metadata <- merge(val_crispr, clade_A1, by = 'Strain')

total_clade_strains <- clade_A1 %>%
  group_by(CladeA1) %>%
  count()


crispr_systems %>%
  group_by(CladeA1) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["CladeA1"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"Clade_A1","2":"19"},{"1":"Clade_B","2":"12"},{"1":"Non_clade_A1","2":"15"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
results_crispr <- val_crispr_metadata %>%
  group_by(CladeA1) %>%
  count()


clade_B_crispr <- subset(results_crispr$n, results_crispr$CladeA1 == 'Clade_B')
noncladeB_crispr <- subset(results_crispr$n, results_crispr$CladeA1 != 'Clade_B')

clade_B_strains <- subset(total_clade_strains$n, total_clade_strains$CladeA1 == 'Clade_B')
noncladeB_strains <- sum(subset(total_clade_strains$n, total_clade_strains$CladeA1 != 'Clade_B'))


clade_B_noncripsr <- clade_B_strains - clade_B_crispr
nonclade_B_noncripsr <- noncladeB_strains - noncladeB_crispr

fisher_crispr <- rbind(c(clade_B_crispr, clade_B_noncripsr), c(noncladeB_crispr, nonclade_B_noncripsr))
fisher.test(x = fisher_crispr, alternative = 'greater')
```

    ## 
    ##  Fisher's Exact Test for Count Data
    ## 
    ## data:  fisher_crispr
    ## p-value < 2.2e-16
    ## alternative hypothesis: true odds ratio is greater than 1
    ## 95 percent confidence interval:
    ##  102.5211      Inf
    ## sample estimates:
    ## odds ratio 
    ##   604.7625

## RM-systems

``` r
# Using our set of 62 complete genomes 

# RM-systems 

abricate_rm <- read_csv(file = '../Files/rmsystem/abricate_rm.csv')
```

    ## Parsed with column specification:
    ## cols(
    ##   `#FILE` = col_character(),
    ##   SEQUENCE = col_character(),
    ##   START = col_character(),
    ##   END = col_character(),
    ##   GENE = col_character(),
    ##   COVERAGE = col_character(),
    ##   COVERAGE_MAP = col_character(),
    ##   GAPS = col_character(),
    ##   `%COVERAGE` = col_character(),
    ##   `%IDENTITY` = col_character(),
    ##   DATABASE = col_character(),
    ##   ACCESSION = col_character(),
    ##   PRODUCT = col_character()
    ## )

``` r
indexes <- which(abricate_rm$SEQUENCE != 'SEQUENCE')
abricate_rm <- abricate_rm[indexes,]


abricate_rm$subunit <- str_split_fixed(string = abricate_rm$GENE, pattern = '\\:', n = 2)[,2]
abricate_rm$Strain <- str_split_fixed(string = abricate_rm$`#FILE`, pattern = '\\/', n = 2)[,1]

colnames(abricate_rm)[c(9,10)] <- c('Coverage_perc','Identity_perc')


filtered_abricate_rm <- subset(abricate_rm, as.numeric(abricate_rm$Coverage_perc) > 90 & as.numeric(abricate_rm$Identity_perc) > 95)

####### S-subunit

subunit <- subset(filtered_abricate_rm, filtered_abricate_rm$subunit == 'S_subunit')


subunit_metadata <- merge(subunit, clade_A1, by = 'Strain')

results_subunit <- subunit_metadata %>%
  group_by(CladeA1) %>%
  count()


complete_genomes <- read.table(file = '../Files/rmsystem/complete_genomes_names.txt')
colnames(complete_genomes) <- 'Strain'
complete_meta <- merge(complete_genomes, clade_A1, by = 'Strain')


total_meta <- complete_meta %>%
  group_by(CladeA1) %>%
  count()

complete_meta %>%
  group_by(CladeA1, source_characteristic, Strain) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["CladeA1"],"name":[1],"type":["chr"],"align":["left"]},{"label":["source_characteristic"],"name":[2],"type":["chr"],"align":["left"]},{"label":["Strain"],"name":[3],"type":["fctr"],"align":["left"]},{"label":["n"],"name":[4],"type":["int"],"align":["right"]}],"data":[{"1":"Clade_A1","2":"Hospitalized patient","3":"E6020","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E6043","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E6055","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E6975","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E6988","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7025","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7040","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7067","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7070","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7098","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7114","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7160","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7171","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7196","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7199","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7207","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7237","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7240","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7246","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7313","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7356","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7357","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7429","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7441","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7471","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7519","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7591","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7654","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7663","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7933","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E7948","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8014","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8040","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8172","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8195","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8202","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8284","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8290","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8328","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8377","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8407","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8414","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8423","4":"1"},{"1":"Clade_A1","2":"Hospitalized patient","3":"E8440","4":"1"},{"1":"Clade_A1","2":"Pet","3":"E4402","4":"1"},{"1":"Clade_A1","2":"Pet","3":"E4413","4":"1"},{"1":"Clade_A1","2":"Pet","3":"E4438","4":"1"},{"1":"Clade_A1","2":"Pet","3":"E4456","4":"1"},{"1":"Clade_A1","2":"Pet","3":"E4457","4":"1"},{"1":"Clade_A1","2":"Pet","3":"E8691","4":"1"},{"1":"Clade_A1","2":"Pet","3":"E8927","4":"1"},{"1":"Clade_A1","2":"Pig","3":"E1774","4":"1"},{"1":"Non_clade_A1","2":"Non-hospitalized person","3":"E0139","4":"1"},{"1":"Non_clade_A1","2":"Non-hospitalized person","3":"E1334","4":"1"},{"1":"Non_clade_A1","2":"Non-hospitalized person","3":"E2364","4":"1"},{"1":"Non_clade_A1","2":"Non-hospitalized person","3":"E9101","4":"1"},{"1":"Non_clade_A1","2":"Pet","3":"E8481","4":"1"},{"1":"Non_clade_A1","2":"Pet","3":"E8867","4":"1"},{"1":"Non_clade_A1","2":"Pig","3":"E0595","4":"1"},{"1":"Non_clade_A1","2":"Pig","3":"E0656","4":"1"},{"1":"Non_clade_A1","2":"Pig","3":"E2079","4":"1"},{"1":"Non_clade_A1","2":"Poultry","3":"E4227","4":"1"},{"1":"Non_clade_A1","2":"Poultry","3":"E4239","4":"1"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
rm_cladeA1 <- subset(results_subunit$n, results_subunit$CladeA1 == 'Clade_A1')
rm_noncladeA1 <- 0

total_cladeA1 <- subset(total_meta$n, total_meta$CladeA1 == 'Clade_A1')
total_noncladeA1 <- subset(total_meta$n, total_meta$CladeA1 != 'Clade_A1')

nonrm_cladeA1 <- total_cladeA1 - rm_cladeA1
nonrm_noncladeA1 <- total_noncladeA1 - rm_noncladeA1


fisher_rm <- rbind(c(rm_cladeA1, nonrm_cladeA1), c(rm_noncladeA1, nonrm_noncladeA1))

fisher.test(x=fisher_rm,alternative="greater") # We perform a Fisher test using our contingency table
```

    ## 
    ##  Fisher's Exact Test for Count Data
    ## 
    ## data:  fisher_rm
    ## p-value = 0.01648
    ## alternative hypothesis: true odds ratio is greater than 1
    ## 95 percent confidence interval:
    ##  1.489964      Inf
    ## sample estimates:
    ## odds ratio 
    ##        Inf

``` r
############# M-subunit

subunit <- subset(filtered_abricate_rm, filtered_abricate_rm$subunit == 'M_subunit')


subunit_metadata <- merge(subunit, clade_A1, by = 'Strain')

results_subunit <- subunit_metadata %>%
  group_by(CladeA1) %>%
  count()


complete_genomes <- read.table(file = '../Files/rmsystem/complete_genomes_names.txt')
colnames(complete_genomes) <- 'Strain'
complete_meta <- merge(complete_genomes, clade_A1, by = 'Strain')


total_meta <- complete_meta %>%
  group_by(CladeA1) %>%
  count()

rm_cladeA1 <- subset(results_subunit$n, results_subunit$CladeA1 == 'Clade_A1')
rm_noncladeA1 <- subset(results_subunit$n, results_subunit$CladeA1 != 'Clade_A1')

total_cladeA1 <- subset(total_meta$n, total_meta$CladeA1 == 'Clade_A1')
total_noncladeA1 <- subset(total_meta$n, total_meta$CladeA1 != 'Clade_A1')

nonrm_cladeA1 <- total_cladeA1 - rm_cladeA1
nonrm_noncladeA1 <- total_noncladeA1 - rm_noncladeA1


fisher_rm <- rbind(c(rm_cladeA1, nonrm_cladeA1), c(rm_noncladeA1, nonrm_noncladeA1))

fisher.test(x=fisher_rm,alternative="greater") # We perform a Fisher test using our contingency table
```

    ## 
    ##  Fisher's Exact Test for Count Data
    ## 
    ## data:  fisher_rm
    ## p-value = 0.2907
    ## alternative hypothesis: true odds ratio is greater than 1
    ## 95 percent confidence interval:
    ##  0.3879018       Inf
    ## sample estimates:
    ## odds ratio 
    ##   2.035251

``` r
############# R-subunit

subunit <- subset(filtered_abricate_rm, filtered_abricate_rm$subunit == 'R_subunit')


subunit_metadata <- merge(subunit, clade_A1, by = 'Strain')

results_subunit <- subunit_metadata %>%
  group_by(CladeA1) %>%
  count()


complete_genomes <- read.table(file = '../Files/rmsystem/complete_genomes_names.txt')
colnames(complete_genomes) <- 'Strain'
complete_meta <- merge(complete_genomes, clade_A1, by = 'Strain')


total_meta <- complete_meta %>%
  group_by(CladeA1) %>%
  count()

rm_cladeA1 <- subset(results_subunit$n, results_subunit$CladeA1 == 'Clade_A1')
rm_noncladeA1 <- subset(results_subunit$n, results_subunit$CladeA1 != 'Clade_A1')

total_cladeA1 <- subset(total_meta$n, total_meta$CladeA1 == 'Clade_A1')
total_noncladeA1 <- subset(total_meta$n, total_meta$CladeA1 != 'Clade_A1')

nonrm_cladeA1 <- total_cladeA1 - rm_cladeA1
nonrm_noncladeA1 <- total_noncladeA1 - rm_noncladeA1


fisher_rm <- rbind(c(rm_cladeA1, nonrm_cladeA1), c(rm_noncladeA1, nonrm_noncladeA1))

fisher.test(x=fisher_rm,alternative="greater") # We perform a Fisher test using our contingency table
```

    ## 
    ##  Fisher's Exact Test for Count Data
    ## 
    ## data:  fisher_rm
    ## p-value = 0.5596
    ## alternative hypothesis: true odds ratio is greater than 1
    ## 95 percent confidence interval:
    ##  0.1614539       Inf
    ## sample estimates:
    ## odds ratio 
    ##   1.218167

``` r
# Using our entire collection of isolates 


abricate_rm <- read_csv(file = '../Files/rmsystem/rm_collection.csv')
```

    ## Parsed with column specification:
    ## cols(
    ##   `#FILE` = col_character(),
    ##   SEQUENCE = col_character(),
    ##   START = col_character(),
    ##   END = col_character(),
    ##   GENE = col_character(),
    ##   COVERAGE = col_character(),
    ##   COVERAGE_MAP = col_character(),
    ##   GAPS = col_character(),
    ##   `%COVERAGE` = col_character(),
    ##   `%IDENTITY` = col_character(),
    ##   DATABASE = col_character(),
    ##   ACCESSION = col_character(),
    ##   PRODUCT = col_character()
    ## )

``` r
indexes <- which(abricate_rm$SEQUENCE != 'SEQUENCE')
abricate_rm <- abricate_rm[indexes,]


abricate_rm$subunit <- str_split_fixed(string = abricate_rm$GENE, pattern = '\\:', n = 2)[,2]
abricate_rm$Strain <- str_split_fixed(string = abricate_rm$`#FILE`, pattern = '\\.', n = 2)[,1]

colnames(abricate_rm)[c(9,10)] <- c('Coverage_perc','Identity_perc')


filtered_abricate_rm <- subset(abricate_rm, as.numeric(abricate_rm$Coverage_perc) > 90 & as.numeric(abricate_rm$Identity_perc) > 95)


####### S-subunit

subunit <- subset(filtered_abricate_rm, filtered_abricate_rm$subunit == 'S_subunit')


subunit_metadata <- merge(subunit, clade_A1, by = 'Strain')

results_subunit <- subunit_metadata %>%
  group_by(CladeA1) %>%
  count()

total_meta <- clade_A1 %>%
  group_by(CladeA1) %>%
  count()

rm_cladeA1 <- subset(results_subunit$n, results_subunit$CladeA1 == 'Clade_A1')
rm_noncladeA1 <- 0

total_cladeA1 <- subset(total_meta$n, total_meta$CladeA1 == 'Clade_A1')
total_noncladeA1 <- sum(subset(total_meta$n, total_meta$CladeA1 != 'Clade_A1'))

nonrm_cladeA1 <- total_cladeA1 - rm_cladeA1
nonrm_noncladeA1 <- total_noncladeA1 - rm_noncladeA1


fisher_rm <- rbind(c(rm_cladeA1, nonrm_cladeA1), c(rm_noncladeA1, nonrm_noncladeA1))

fisher.test(x=fisher_rm,alternative="greater") # We perform a Fisher test using our contingency table
```

    ## 
    ##  Fisher's Exact Test for Count Data
    ## 
    ## data:  fisher_rm
    ## p-value < 2.2e-16
    ## alternative hypothesis: true odds ratio is greater than 1
    ## 95 percent confidence interval:
    ##  73.59852      Inf
    ## sample estimates:
    ## odds ratio 
    ##        Inf

``` r
subunit_metadata %>%
  group_by(CladeA1, source_characteristic) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["CladeA1"],"name":[1],"type":["chr"],"align":["left"]},{"label":["source_characteristic"],"name":[2],"type":["chr"],"align":["left"]},{"label":["n"],"name":[3],"type":["int"],"align":["right"]}],"data":[{"1":"Clade_A1","2":"Hospitalized patient","3":"394"},{"1":"Clade_A1","2":"Human","3":"1"},{"1":"Clade_A1","2":"Non-hospitalized person","3":"3"},{"1":"Clade_A1","2":"Pet","3":"4"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
subunit_metadata %>%
  group_by(CladeA1) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["CladeA1"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"Clade_A1","2":"402"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
####### M-subunit

subunit <- subset(filtered_abricate_rm, filtered_abricate_rm$subunit == 'M_subunit')


subunit_metadata <- merge(subunit, clade_A1, by = 'Strain')

results_subunit <- subunit_metadata %>%
  group_by(CladeA1) %>%
  count()

total_meta <- clade_A1 %>%
  group_by(CladeA1) %>%
  count()

rm_cladeA1 <- subset(results_subunit$n, results_subunit$CladeA1 == 'Clade_A1')
rm_noncladeA1 <- sum(subset(results_subunit$n, results_subunit$CladeA1 != 'Clade_A1'))

total_cladeA1 <- subset(total_meta$n, total_meta$CladeA1 == 'Clade_A1')
total_noncladeA1 <- sum(subset(total_meta$n, total_meta$CladeA1 != 'Clade_A1'))

nonrm_cladeA1 <- total_cladeA1 - rm_cladeA1
nonrm_noncladeA1 <- total_noncladeA1 - rm_noncladeA1


fisher_rm <- rbind(c(rm_cladeA1, nonrm_cladeA1), c(rm_noncladeA1, nonrm_noncladeA1))

fisher.test(x=fisher_rm,alternative="greater") # We perform a Fisher test using our contingency table
```

    ## 
    ##  Fisher's Exact Test for Count Data
    ## 
    ## data:  fisher_rm
    ## p-value = 4.545e-06
    ## alternative hypothesis: true odds ratio is greater than 1
    ## 95 percent confidence interval:
    ##  1.37008     Inf
    ## sample estimates:
    ## odds ratio 
    ##    1.65806

``` r
subunit_metadata %>%
  group_by(CladeA1) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["CladeA1"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"Clade_A1","2":"828"},{"1":"Clade_B","2":"15"},{"1":"Non_clade_A1","2":"239"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
####### R-subunit

subunit <- subset(filtered_abricate_rm, filtered_abricate_rm$subunit == 'R_subunit')


subunit_metadata <- merge(subunit, clade_A1, by = 'Strain')

results_subunit <- subunit_metadata %>%
  group_by(CladeA1) %>%
  count()

total_meta <- clade_A1 %>%
  group_by(CladeA1) %>%
  count()

rm_cladeA1 <- subset(results_subunit$n, results_subunit$CladeA1 == 'Clade_A1')
rm_noncladeA1 <- sum(subset(results_subunit$n, results_subunit$CladeA1 != 'Clade_A1'))

total_cladeA1 <- subset(total_meta$n, total_meta$CladeA1 == 'Clade_A1')
total_noncladeA1 <- sum(subset(total_meta$n, total_meta$CladeA1 != 'Clade_A1'))

nonrm_cladeA1 <- total_cladeA1 - rm_cladeA1
nonrm_noncladeA1 <- total_noncladeA1 - rm_noncladeA1


fisher_rm <- rbind(c(rm_cladeA1, nonrm_cladeA1), c(rm_noncladeA1, nonrm_noncladeA1))

fisher.test(x=fisher_rm,alternative="greater") # We perform a Fisher test using our contingency table
```

    ## 
    ##  Fisher's Exact Test for Count Data
    ## 
    ## data:  fisher_rm
    ## p-value = 0.0004983
    ## alternative hypothesis: true odds ratio is greater than 1
    ## 95 percent confidence interval:
    ##  1.208705      Inf
    ## sample estimates:
    ## odds ratio 
    ##    1.46825

``` r
subunit_metadata %>%
  group_by(CladeA1, source_characteristic) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["CladeA1"],"name":[1],"type":["chr"],"align":["left"]},{"label":["source_characteristic"],"name":[2],"type":["chr"],"align":["left"]},{"label":["n"],"name":[3],"type":["int"],"align":["right"]}],"data":[{"1":"Clade_A1","2":"Hospitalized patient","3":"770"},{"1":"Clade_A1","2":"Human","3":"1"},{"1":"Clade_A1","2":"Non-hospitalized person","3":"15"},{"1":"Clade_A1","2":"Pet","3":"64"},{"1":"Clade_A1","2":"Pig","3":"1"},{"1":"Clade_A1","2":"Wild reservoir","3":"2"},{"1":"Clade_B","2":"Hospitalized patient","3":"1"},{"1":"Clade_B","2":"Non-hospitalized person","3":"10"},{"1":"Clade_B","2":"Wild reservoir","3":"3"},{"1":"Non_clade_A1","2":"Food","3":"1"},{"1":"Non_clade_A1","2":"Hospitalized patient","3":"21"},{"1":"Non_clade_A1","2":"Human","3":"3"},{"1":"Non_clade_A1","2":"Non-hospitalized person","3":"50"},{"1":"Non_clade_A1","2":"Pet","3":"50"},{"1":"Non_clade_A1","2":"Pig","3":"68"},{"1":"Non_clade_A1","2":"Poultry","3":"67"},{"1":"Non_clade_A1","2":"Wild reservoir","3":"4"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

# Plasmid contribution

## Loading and scaling Mash distances of whole-genome, plasmid-predicted and chromosome-derived contigs

``` r
bionj_function <- function(tree)
{
  parsing_mash <- read.table(tree)
  strains <- parsing_mash[,1]
  mash_df <- parsing_mash[,c(2:1640)]
  
  strains <- gsub('.fasta.gz','',strains)
  
  colnames(mash_df) <- strains
  rownames(mash_df) <- strains
  
  mash_df <<- mash_df[! rownames(mash_df) %in% strains_excluded, ! colnames(mash_df) %in% strains_excluded]
  
  # BIONJ tree estimation ape package
  
  tr_mash_bionj <<- midpoint(bionj(as.matrix(mash_df)))
}


# Chromosome Pairwise distances

bionj_function(tree = '../Files/Phylogenies/chromosome_predicted/mash_distances.txt')
chr_tree <- tr_mash_bionj

chr_mash <- mash_df
chr_mash$Strain <- rownames(chr_mash)


chr_gather_mash <- chr_mash %>%
  gather(key = Pairwise_isolate, value = Mash_Distance, -Strain)

chr_gather_mash$Mash_scaled <- scale(chr_gather_mash$Mash_Distance, scale = TRUE, center = TRUE)
chr_gather_mash$Type <- 'Chromosome'

# Plasmid Pairwise distances
  
plasmid_mash <- polished_mash_df
plasmid_mash$Strain <- rownames(plasmid_mash)

pl_gather_mash <- plasmid_mash %>%
  gather(key = Pairwise_isolate, value = Mash_Distance, -Strain)


pl_gather_mash$Mash_scaled <- scale(pl_gather_mash$Mash_Distance, scale = TRUE, center = TRUE)
pl_gather_mash$Type <- 'Plasmid'


# Whole-genome distances

bionj_function(tree = '../Files/Phylogenies/whole_genome/mash_distances.txt')
whole_mash <- mash_df
whole_mash$Strain <- rownames(whole_mash)

whole_gather_mash <- whole_mash %>%
  gather(key = Pairwise_isolate, value = Mash_Distance, -Strain)

whole_gather_mash$Mash_scaled <- scale(whole_gather_mash$Mash_Distance, scale = TRUE, center = TRUE)
whole_gather_mash$Type <- 'Whole-Genome'


# Joining everything in a single dataframe

all_gather_mash <- rbind(chr_gather_mash, pl_gather_mash)
all_gather_mash <- rbind(all_gather_mash, whole_gather_mash)

index_others <- which(metadata$isolation.source %in% c('Hospitalized patient','Pig','Chicken','Dog','Non-hospitalized person') == FALSE)
metadata$isolation.source[index_others] <- 'Others'

isol_metadata <- subset(metadata, select = c('Strain','isolation.source'))

all_gather_metadata <- merge(all_gather_mash, isol_metadata, by = 'Strain')
colnames(isol_metadata)[1] <- 'Pairwise_isolate'

all_gather_metadata <- merge(all_gather_metadata, isol_metadata, by = 'Pairwise_isolate')


#write.csv(x = all_gather_metadata, file = '../Files/Plasmid_contribution/mash_full_scaled.csv', quote = FALSE, row.names = FALSE)


whole_gather_metadata <- merge(whole_gather_mash, isol_metadata, by = 'Strain')
colnames(isol_metadata)[1] <- 'Pairwise_isolate'

whole_gather_metadata <- merge(whole_gather_metadata, isol_metadata, by = 'Pairwise_isolate')

#write.csv(x = whole_gather_metadata, file = '../Files/Plasmid_contribution/mash_whole_genome_scaled.csv', quote = FALSE, row.names = FALSE)
```

``` r
whole_gather_metadata <- read.csv(file = '../Files/Plasmid_contribution/mash_whole_genome_scaled.csv',header = TRUE)

full_gather_metadata <- rbind(all_gather_metadata, whole_gather_metadata)

(density_full_all_hosts <- ggplot(full_gather_metadata, aes(Mash_scaled, color = isolation.source.y, alpha = 0.01)) + xlim(min(full_gather_metadata$Mash_scaled),2.5) + geom_density() + scale_color_manual(values = c('#b8860b','#9acd32','#dc143c','#0000ff','black','#ff69b4')) + facet_wrap(full_gather_metadata$isolation.source.x ~ full_gather_metadata$Type))
```

``` r
# Bootstrap approach 
boot_func <- function(input_df, type, host)
{
all_iterations <- NULL
input_df$pair_pair <- paste(input_df$Pairwise_isolate, input_df$Strain, sep = '-')
for(iter in 1:100)
{
hosp_set <- subset(input_df, input_df$isolation.source.y == 'Hospitalized patient' & input_df$Type == type)
dog_set <- subset(input_df, input_df$isolation.source.y == 'Dog' & input_df$Type == type)
pig_set <- subset(input_df, input_df$isolation.source.y == 'Pig' & input_df$Type == type)
poultry_set <- subset(input_df, input_df$isolation.source.y == 'Chicken' & input_df$Type == type)
non_hosp_set <- subset(input_df, input_df$isolation.source.y == 'Non-hospitalized person' & input_df$Type == type)

test_hosp <- as.character(sample(hosp_set$pair_pair, 50, replace = TRUE))
test_dog <- as.character(sample(dog_set$pair_pair, 50, replace = TRUE))
test_pig <- as.character(sample(pig_set$pair_pair, 50, replace = TRUE))
test_poultry <- as.character(sample(poultry_set$pair_pair, 50, replace = TRUE))
test_non_hosp <- as.character(sample(non_hosp_set$pair_pair, 50, replace = TRUE))


hosp_sample <- subset(hosp_set, hosp_set$pair_pair %in% test_hosp)
dog_sample <- subset(dog_set, dog_set$pair_pair %in% test_dog)
pig_sample <- subset(pig_set, pig_set$pair_pair %in% test_pig)
poultry_sample <- subset(poultry_set, poultry_set$pair_pair %in% test_poultry)
non_hosp_sample <- subset(non_hosp_set, non_hosp_set$pair_pair %in% test_non_hosp)


all_samples <- rbind(hosp_sample, dog_sample)
all_samples <- rbind(all_samples, pig_sample)
all_samples <- rbind(all_samples, poultry_sample)
all_samples <- rbind(all_samples, non_hosp_sample)

summary_df <- group_by(all_samples, isolation.source.y) %>%
  summarise(
    mean = mean(Mash_scaled, na.rm = TRUE),
    median = median(Mash_scaled, na.rm = TRUE),
    sd = sd(Mash_scaled, na.rm = TRUE)
  )

summary_df$iteration <- iter
summary_df$Type <- type
summary_df$Host <- host

#write.table(x = summary_df, file = '../Files/Plasmid_contribution/diff_whole_genome_distribution.table', append = TRUE, row.names = FALSE, col.names = FALSE)
}
}

boot_func(dog_gather_metadata, 'Plasmid','Dog')
boot_func(dog_gather_metadata, 'Chromosome','Dog')

boot_func(hosp_gather_metadata, 'Plasmid','Hospitalized patient')
boot_func(hosp_gather_metadata, 'Chromosome','Hospitalized patient')

boot_func(pig_gather_metadata, 'Plasmid','Pig')
boot_func(pig_gather_metadata, 'Chromosome','Pig')

boot_func(poultry_gather_metadata, 'Plasmid','Chicken')
boot_func(poultry_gather_metadata, 'Chromosome','Chicken')

boot_func(non_hosp_gather_metadata, 'Plasmid','Non-hospitalized person')
boot_func(non_hosp_gather_metadata, 'Chromosome','Non-hospitalized person')


boot_func <- function(input_df, type, host)
{
all_iterations <- NULL
input_df$pair_pair <- paste(input_df$Pairwise_isolate, input_df$Strain, sep = '-')
for(iter in 1:100)
{
hosp_set <- subset(input_df, input_df$isolation.source.y == 'Hospitalized patient' & input_df$Type == type)
dog_set <- subset(input_df, input_df$isolation.source.y == 'Dog' & input_df$Type == type)
pig_set <- subset(input_df, input_df$isolation.source.y == 'Pig' & input_df$Type == type)
poultry_set <- subset(input_df, input_df$isolation.source.y == 'Chicken' & input_df$Type == type)
non_hosp_set <- subset(input_df, input_df$isolation.source.y == 'Non-hospitalized person' & input_df$Type == type)

test_hosp <- as.character(sample(hosp_set$pair_pair, 100, replace = TRUE))
test_dog <- as.character(sample(dog_set$pair_pair, 100, replace = TRUE))
test_pig <- as.character(sample(pig_set$pair_pair, 100, replace = TRUE))
test_poultry <- as.character(sample(poultry_set$pair_pair, 100, replace = TRUE))
test_non_hosp <- as.character(sample(non_hosp_set$pair_pair, 100, replace = TRUE))


concat_samples <- c(test_hosp, test_dog, test_pig, test_poultry, test_non_hosp)

random_samples <- sample(concat_samples, 50, replace = TRUE)

all_samples <- subset(input_df, input_df$pair_pair %in% random_samples)

all_samples$isolation.source.y <- 'Random_group'

summary_df <- group_by(all_samples, isolation.source.y) %>%
  summarise(
    mean = mean(Mash_scaled, na.rm = TRUE),
    median = median(Mash_scaled, na.rm = TRUE),
    sd = sd(Mash_scaled, na.rm = TRUE)
  )

summary_df$iteration <- iter
summary_df$Type <- type
summary_df$Host <- host

#write.table(x = summary_df, file = '../Files/Plasmid_contribution/diff_whole_genome_distribution.table', append = TRUE, row.names = FALSE, col.names = FALSE)
}
}

boot_func(dog_gather_metadata, 'Plasmid','Dog')
boot_func(dog_gather_metadata, 'Chromosome','Dog')

boot_func(pig_gather_metadata, 'Plasmid','Pig')
boot_func(pig_gather_metadata, 'Chromosome','Pig')

boot_func(hosp_gather_metadata, 'Plasmid','Hospitalized patient')
boot_func(hosp_gather_metadata, 'Chromosome','Hospitalized patient')

boot_func(poultry_gather_metadata, 'Plasmid','Poultry')
boot_func(poultry_gather_metadata, 'Chromosome','Poultry')

boot_func(non_hosp_gather_metadata, 'Plasmid','Non-hospitalized person')
boot_func(non_hosp_gather_metadata, 'Chromosome','Non-hospitalized person')

# Whole-genome

boot_func(wh_dog_gather_metadata, 'Whole-Genome','Dog')
boot_func(wh_hosp_gather_metadata, 'Whole-Genome','Hospitalized patient')
boot_func(wh_pig_gather_metadata, 'Whole-Genome','Pig')
boot_func(wh_poultry_gather_metadata, 'Whole-Genome','Chicken')
boot_func(wh_non_hosp_gather_metadata, 'Whole-Genome','Non-hospitalized person')
```

## Plots of the distribution

``` r
# Graphs 
results_table <- read.table(file = '../Files/Plasmid_contribution/diff_distribution.table')
results_wg_table <- read.table(file = '../Files/Plasmid_contribution/diff_whole_genome_distribution.table')

results_table <- rbind(results_table, results_wg_table)

colnames(results_table) <- c('Isolation_source','mean','median','sd','iteration','type','host')

poultry <- which(results_table$host == 'Chicken')
results_table$host <- as.character(results_table$host)
results_table$host[poultry] <- 'Poultry'

poultry <- which(results_table$Isolation_source == 'Chicken')
results_table$Isolation_source <- as.character(results_table$Isolation_source)
results_table$Isolation_source[poultry] <- 'Poultry'

(bootstrap_density <- ggplot(results_table, aes(mean, color = Isolation_source, fill = Isolation_source, alpha = 0.1)) + geom_density() + scale_fill_manual(values = c('#9acd32','#dc143c','#0000ff','#ff69b4','#b8860b','black')) + scale_colour_manual(values = c('#9acd32','#dc143c','#0000ff','#ff69b4','#b8860b','black')) + facet_wrap(results_table$host ~ results_table$type, ncol = 3) + theme_bw() + labs(xlab = 'Average pairwise distances', fill = 'Isolation source', color = 'Isolation source') + theme(axis.text.x = element_text(angle = 90, hjust = 1)) + theme(legend.position = 'top')) 
```

![](analysis_files/figure-gfm/unnamed-chunk-32-1.png)<!-- -->

## Assessing differences of within-hosts and between-hosts group against our random group

### Anova-test

``` r
host_results <- subset(results_table, results_table$host == 'Pig') # We did that for each host
res_aov <- aov(mean ~ Isolation_source, data = host_results)
results_anova <- TukeyHSD(res_aov)
results_anova_df <- as.data.frame(results_anova$Isolation_source)
results_anova_df$type <- 'Chromosome' # And for each type
results_anova_df$host <- 'Pig'
# write.table(x = results_anova_df, file = '../Files/Plasmid_contribution/results_anova_tests.table', append = TRUE, row.names = TRUE, col.names = FALSE)
```

### Plot summary

``` r
# Checking if there are differences between the distribution of plasmid and whole-genome 

# Obtaining the results using a one-way test ANOVA

all_anova_tests <- read.table(file = '../Files/Plasmid_contribution/results_anova_tests.table')
all_anova_tests <- separate(all_anova_tests, V1, into = c('First','Second'),sep = '-')


all_hosts <- NULL

for(host in c('Hospitalized patient','Dog','Chicken','Pig','Non hospitalized person'))
{

host_anovas <- subset(all_anova_tests, all_anova_tests$V7 == host)
host_anovas <- subset(host_anovas, host_anovas$First == 'Random_group')
all_hosts <<- rbind(all_hosts, host_anovas)
}

poultry <- which(all_hosts$V7 == 'Chicken')
all_hosts$V7 <- as.character(all_hosts$V7)
all_hosts$V7[poultry] <- 'Poultry'

sig <- which(all_hosts$V5 < 0.05)
all_hosts$Significance <- 'Non significant'
all_hosts$Significance[sig] <- 'Significant'

(overview_plot <- ggplot(all_hosts, aes(x = Second, y = V2, group = V6, shape = Significance)) + geom_point(aes(color=V6, fill = V6), size = 3.0) + geom_line(aes(linetype = V6, color = V6)) + labs(colour = 'Genomic component', xlab = '', y = 'Observed difference parwise mean', linetype = 'Genomic component', fill = 'Genomic component', shape = 'Significance') +  theme_bw() + geom_hline(aes(yintercept = 0.0)) + scale_color_manual(values = c('coral2','purple','black','magenta')) +  scale_fill_manual(values = c('coral2','purple','black','magenta'))  + scale_shape_manual(values = c(17,19))+ theme(legend.position = 'top') + scale_linetype_manual(values=c("dotted","twodash", "solid",'F1')) + facet_wrap(~ all_hosts$V7, nrow = 3, ncol = 2)) 
```

![](analysis_files/figure-gfm/unnamed-chunk-34-1.png)<!-- -->

## Including time and geographical position as covariates to explain the observed Mash distances

``` r
pops_info <- read.csv(file = '../Files/Metadata/diff_pop.csv')
all_distances_info <- read.csv(file = '../Files/Plasmid_contribution/all_distances_info.csv')
all_distances_info$Source <- ifelse(all_distances_info$Source == 'Different_source',0,1)
all_distances_info <- merge(all_distances_info, pops_info, by = 'Pair')


set.seed(123)

summary(lm(Mash_Distance ~ Source, data = all_distances_info))
```

    ## 
    ## Call:
    ## lm(formula = Mash_Distance ~ Source, data = all_distances_info)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -0.07228 -0.01004 -0.00016  0.00890  0.92772 
    ## 
    ## Coefficients:
    ##               Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)  7.228e-02  1.915e-05    3773   <2e-16 ***
    ## Source      -2.983e-02  2.514e-05   -1186   <2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.01834 on 2185960 degrees of freedom
    ## Multiple R-squared:  0.3917, Adjusted R-squared:  0.3917 
    ## F-statistic: 1.408e+06 on 1 and 2185960 DF,  p-value: < 2.2e-16

``` r
summary(lm(Mash_Distance ~ Geographical_Distance, data = all_distances_info))
```

    ## 
    ## Call:
    ## lm(formula = Mash_Distance ~ Geographical_Distance, data = all_distances_info)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -0.05493 -0.01573 -0.00381  0.01746  0.94469 
    ## 
    ## Coefficients:
    ##                        Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)           5.475e-02  1.954e-05 2802.17   <2e-16 ***
    ## Geographical_Distance 2.573e-10  1.351e-11   19.05   <2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.02352 on 2185960 degrees of freedom
    ## Multiple R-squared:  0.000166,   Adjusted R-squared:  0.0001656 
    ## F-statistic:   363 on 1 and 2185960 DF,  p-value: < 2.2e-16

``` r
summary(lm(Mash_Distance ~ Year_Difference, data = all_distances_info))
```

    ## 
    ## Call:
    ## lm(formula = Mash_Distance ~ Year_Difference, data = all_distances_info)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -0.09137 -0.01188 -0.00119  0.01074  0.94610 
    ## 
    ## Coefficients:
    ##                  Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     4.211e-02  1.917e-05  2196.5   <2e-16 ***
    ## Year_Difference 2.359e-03  2.511e-06   939.3   <2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.01985 on 2185960 degrees of freedom
    ## Multiple R-squared:  0.2875, Adjusted R-squared:  0.2875 
    ## F-statistic: 8.822e+05 on 1 and 2185960 DF,  p-value: < 2.2e-16

``` r
model_distances <- lm(Mash_Distance ~ Source + Geographical_Distance + Year_Difference, data = all_distances_info)
summary(model_distances)
```

    ## 
    ## Call:
    ## lm(formula = Mash_Distance ~ Source + Geographical_Distance + 
    ##     Year_Difference, data = all_distances_info)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -0.08253 -0.00980 -0.00056  0.00852  0.93122 
    ## 
    ## Coefficients:
    ##                         Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)            6.173e-02  3.145e-05 1962.87   <2e-16 ***
    ## Source                -2.299e-02  3.041e-05 -755.97   <2e-16 ***
    ## Geographical_Distance  7.112e-10  1.033e-11   68.81   <2e-16 ***
    ## Year_Difference        1.097e-03  2.805e-06  391.07   <2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.01766 on 2185958 degrees of freedom
    ## Multiple R-squared:  0.436,  Adjusted R-squared:  0.436 
    ## F-statistic: 5.633e+05 on 3 and 2185958 DF,  p-value: < 2.2e-16

``` r
library(lmPerm)

permutation_model_distances <- lmp(Mash_Distance ~ Geographical_Distance + Year_Difference, data = all_distances_info, perm = 'Prob')

load('~/Data/efaecium_population/Files/Plasmid_contribution/permutation_test.Rdata')
```
